#include "gameData.h"
#include "ecs.h"
#include "entityMap.h"
#include "physics.h"
#include "playerCamera.h"
#include "chunkMap.h"

#ifndef WORLD_H
#define WORLD_H

// Eat my shorts!!!

// The fucking world.
typedef struct World {
	cpSpace * physicsSpace;
	EcsData ecsData;
	EntityMap entityMap;
	Camera2D playerCamera;
	ChunkMap chunkMap;
} World;

// Many fucking worlds.
typedef struct WorldList {
	size_t count;
	int current;
	World * worlds;
} WorldList;

// World stuff.
World createWorld(size_t entityCount);
void freeWorld(World * world);

// World list shit.
WorldList createWorldList(size_t count);
void freeWorldList(WorldList * worlds);

#endif
