#include "world.h"
#include "game.h"
#include "util.h"

World createWorld(size_t entityCount) {
	World world;

	world.physicsSpace = cpSpaceNew();
	initEcs(&world.ecsData, entityCount);
	initEntityMap(&world.entityMap);
	initPlayerCamera(&world.playerCamera);
	world.chunkMap = getEmptyChunkMap();

	return world;
}

void freeWorld(World * world) {
	closeEcs(&world->ecsData);
	closeEntityMap(&world->entityMap);
	cpSpaceFree(world->physicsSpace);
	freeChunkMap(&world->chunkMap);
	world->physicsSpace = NULL;
}

WorldList createWorldList(size_t count) {
	WorldList worldList;

	worldList.count = count;
	worldList.current = 0;
	
	// Allocate.
	worldList.worlds = (World*)calloc(count, sizeof(World));

	if (worldList.worlds == NULL)
		allocationError("worldList.worlds");

	return worldList;
}

void freeWorldList(WorldList * worlds) {
	int i;

	for (i = 0; i < worlds->count; ++i) {
		TraceLog(LOG_INFO, "Freeing world %d", i);
		freeWorld(&worlds->worlds[i]);
		TraceLog(LOG_INFO, "World %d freed", i);
	}

	free(worlds->worlds);
	worlds->count = 0;
	worlds->current = 0;
	worlds->worlds = NULL;
}
