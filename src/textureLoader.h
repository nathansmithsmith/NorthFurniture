#include "gameData.h"

#ifndef IMAGE_LOADER_H
#define IMAGE_LOADER_H

// Default ids.
enum {
	LOGO_TEXTURE
};

typedef struct GameTextureData {
	Texture2D * textures;
	size_t texturesSize;
} GameTextureData;

typedef struct TextureComponent {
	Texture2D * texture; // Pointer to a texture in 'GameTextureData'.
	int id; // Its id in 'GameTextureData'.
} TextureComponent;

// Starts texture data with one allocated texture.
void initTextureData(GameTextureData * textureData);
void closeTextureData(GameTextureData * textureData);

ErrorCodes addTextures(GameTextureData * textureData, const Texture2D * textures, size_t texturesSize);

// Returns the texture at 'id' in 'textures'.
Texture2D getTexture(GameTextureData textureData, int id);

ErrorCodes loadTexturesFromFiles(GameTextureData * textureData, const char ** files, size_t filesSize);

// Creates a texture component indeed.
// Needs to be freed )-:
TextureComponent * createTextureComponent();
void freeTextureComponent(TextureComponent * component);
void setTextureComponent(TextureComponent * textureComponent, GameTextureData textureData, int id);

#endif
