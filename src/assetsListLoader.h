#include "gameData.h"

#ifndef ASSETS_LIST_LOADER_H
#define ASSETS_LIST_LOADER_H

#define ASSETS_LIST_NAME_MAX 255

typedef struct AssetsList {
	char ** files;
	size_t filesCount;
} AssetsList;

void initAssetsList(AssetsList * assetsList);
void closeAssetsList(AssetsList * assetsList);

// Adds 'fileName' to 'assetsList'.
// 'fileNameSize' needs to include ending null byte.
ErrorCodes addAssetFile(AssetsList * assetsList, const char * fileName, size_t fileNameSize);

ErrorCodes loadAssetFilesFromBuf(AssetsList * assetsList, const char * buf, size_t bufSize);

#endif
