#include "chunkMap.h"
#include "game.h"
#include "world.h"

ChunkMap getEmptyChunkMap() {
	return (ChunkMap){
		.chunks = (EntityList2d){NULL, 0}
	};
}

ChunkMap createChunkMap(int width, int height) {
	ChunkMap chunkMap;

	if (createEntityList2d(&chunkMap.chunks, width, height) != CSUCCESS)
		return getEmptyChunkMap();

	return chunkMap;
}

void freeChunkMap(ChunkMap * chunkMap) {
	freeEntityList2d(&chunkMap->chunks);
	*chunkMap = getEmptyChunkMap();
}

void updateChunkMap(ChunkMap * chunkMap, World * world, GameData * gameData) {
	Camera2D camera = world->playerCamera;
	EntityList2d chunks = chunkMap->chunks;

	int x, y;
	int startX, startY; // Where loop should start.
	int endX, endY; // Where loop should end.

	// Get area.
	Rectangle area = (Rectangle){
		(camera.target.x - camera.offset.x) / CHUNK_PIXEL_SIZE,
		(camera.target.y - camera.offset.y) / CHUNK_PIXEL_SIZE,
		(CHUNK_SCREEN_WIDTH / camera.zoom) + 1,
		(CHUNK_SCREEN_HEIGHT/ camera.zoom) + 1
	};

	//printf("%f %f %f %f\n", area.x, area.y, area.width, area.height);

	// Set start values.
	startX = area.x;
	startY = area.y;

	// Set end values.
	endX = area.x + area.width;
	endY = area.y + area.height;

	// Clamp them.
	startX = Clamp(startX, 0, chunks.width - 1);
	startY = Clamp(startY, 0, chunks.height - 1);
	endX = Clamp(endX, 0, chunks.width - 1);
	endY = Clamp(endY, 0, chunks.height - 1);

	// Run systems.
	for (y = startY; y < endY; ++y)
		for (x = startX; x < endX; ++x) {
			chunkMap->currDrawPos = (Vector2){x * CHUNK_PIXEL_SIZE, y * CHUNK_PIXEL_SIZE};

			runSystems(
				gameData,
				world,
				getEntityFromList2d(chunks, (Vector2){x, y})
			);
		}
}
