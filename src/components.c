#include "components.h"
#include "util.h"
#include "worldBufferLoading/componentToFromBuffer.h"

EntityComponents getEmptyEntityComponents() {
	EntityComponents emplyComponent = {
		.position = NULL,
		.angle = 0,
		.texture = NULL,
		.rect = NULL, .physics = NULL,
		.chunk = NULL
	};

	return emplyComponent;
}

int createComponentsFromSignature(EntityComponents * components, EntitySignature signature) {
	int componentsFailed = 0; // Tracking errors when creating components.

	// Add components.
	if (HAS_FLAG(signature, POSITION_COMPONENT)) {
		components->position = (Vector2*)malloc(sizeof(Vector2));
		componentsFailed += (components->position == NULL);
	} if (HAS_FLAG(signature, ANGLE_COMPONENT)) {
		// Nothing needed 😼
	} if (HAS_FLAG(signature, TEXTURE_COMPONENT)) {
		// Up to user to set  
		// But memory is waiting for the user.
		components->texture = createTextureComponent();
		componentsFailed += (components->texture == NULL);
	} if (HAS_FLAG(signature, RECT_COMPONENT)) {
		components->rect = (Rectangle*)malloc(sizeof(Rectangle));
		componentsFailed += (components->rect == NULL);
	} if (HAS_FLAG(signature, PHYSICS_COMPONENT)) {
		components->physics = createPhysicsComponent();
		componentsFailed += (components->physics == NULL);
	} if (HAS_FLAG(signature, CHUNK_COMPONENT)) {
		components->chunk = createChunkComponent();
		componentsFailed += (components->chunk == NULL);
	}
	
	return componentsFailed;
}

void freeEntityComponents(EntityComponents * components) {
	// Free memory.
	// Texture is freed by the texture loader.
	if (components->position != NULL)
		free(components->position);
	if (components->rect != NULL)
		free(components->rect);
	if (components->texture != NULL)
		freeTextureComponent(components->texture);
	if (components->physics != NULL)
		freePhysicsComponent(components->physics);
	if (components->chunk != NULL)
		freeChunkComponent(components->chunk);

	*components = getEmptyEntityComponents();
}
