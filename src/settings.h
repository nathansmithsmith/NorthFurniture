#include "gameData.h"
#include "configLoader.h"

#ifndef SETTINGS_H
#define SETTINGS_H

// Default size for window.
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

// All the settings in the game.
typedef struct Settings {

	// Screen.
	int windowWidth;
	int windowHeight;
	bool fullScreen;

	// Fps.
	bool vsync;
	int targetFps; // 0 for max.
	bool drawFps;

	// Graphics.
	bool integerScaling;

	// Data used for reading and writing settings to a config file.
	size_t configLinesSize;
	ConfigLine * configLines;

} Settings;

extern const Settings defaultSettings;

void initSettings(Settings * settings);
void closeSettings(Settings * settings);

// Use these to read and write settings to config file.
ConfigErrors dumpSettings(const char * filePath, const Settings * settings);

// Loads and applys settings.
ConfigErrors loadSettings(GameData * gameData, const char * filePath, Settings * settings);

void applySettings(GameData * gameData, Settings * settings);

#endif
