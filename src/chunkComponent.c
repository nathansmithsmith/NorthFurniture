#include "chunkComponent.h"
#include "util.h"

ChunkComponent * createChunkComponent() {
	ChunkComponent * component = (ChunkComponent*)malloc(sizeof(ChunkComponent));

	if (component == NULL) {
		allocationError("ChunkComponent");
		return NULL;
	}

	component->textureId = -1;
	return component;
}

void freeChunkComponent(ChunkComponent * component) {
	free(component);
}
