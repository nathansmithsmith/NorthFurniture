#include "entitySystems.h"
#include "util.h"
#include "chunkComponent.h"
#include "game.h"
#include "chunkMap.h"

void drawSystem(SystemArgs args) {
	DrawTextureV(
		*args.componentData->texture->texture,
		*args.componentData->position,
		WHITE
	);
}

void drawSystemRect(SystemArgs args) {
	DrawTextureRec(
		*args.componentData->texture->texture,
		*args.componentData->rect,
		(Vector2){
			args.componentData->position->x - args.componentData->rect->width / 2.0,
			args.componentData->position->y - args.componentData->rect->height / 2.0
		},
		WHITE
	);
}

void drawChunkSystem(SystemArgs args) {
	int i;
	int x, y;
	int pos;

	// Get texture.
	Texture2D texture = getTexture(
		args.gameData->textureData,
		args.componentData->chunk->textureId
	);

	Rectangle rect;
	Vector2 drawPos;
	i = 0;

	int width = texture.width / TILE_SIZE;

	for (y = 0; y < CHUNK_SIZE; ++y)
		for (x = 0; x < CHUNK_SIZE; ++x) {
			// Get current position.
			pos = args.componentData->chunk->positions[i];

			// Get rect from 'pos'
			rect = (Rectangle){
				(pos % width) * TILE_SIZE,
				floorf((float)pos / width) * TILE_SIZE,
				TILE_SIZE,
				TILE_SIZE
			};

			drawPos = (Vector2){
				x * TILE_SIZE + args.world->chunkMap.currDrawPos.x,
				y * TILE_SIZE + args.world->chunkMap.currDrawPos.y
			};

			// Draw indeed.
			DrawTextureRec(
				texture,
				rect,
				drawPos,
				WHITE
			);

			++i;
		}
}

void testSystem(SystemArgs args) {
	//args.componentData->position->x += 0.1;
	//args.componentData->position->y += 0.1;
}

void physicsSystem(SystemArgs args) {
	if (args.componentData->physics->body == NULL)
		return;

	cpVect pos = cpBodyGetPosition(args.componentData->physics->body);
	*args.componentData->position = (Vector2){pos.x, pos.y};
}

// Default systems.
const EntitySystemEntry defaultEntitySystemTable[] = {
	{"drawSystem", drawSystem},
	{"drawSystemRect", drawSystemRect},
	{"drawChunkSystem", drawChunkSystem},
	{"testSystem", testSystem},
	{"physicsSystem", physicsSystem}
};

const size_t defaultEntitySystemTableSize = sizeof(defaultEntitySystemTable) 
	/ sizeof(EntitySystemEntry);

// Functions for entity system table.

ErrorCodes initEntitySystemTable(EntitySystemTable * table) {
	ErrorCodes res = CSUCCESS;

	// Allocate memory.
	res = createEntitySystemTable(table, defaultEntitySystemTableSize);

	if (res != CSUCCESS)
		return res;

	// Copy default over systems.
	memcpy(table->systems, defaultEntitySystemTable, sizeof(defaultEntitySystemTable));

	TraceLog(LOG_INFO, "Entity system table created with %ld default systems", defaultEntitySystemTableSize);
	return res;
}

ErrorCodes createEntitySystemTable(EntitySystemTable * table, size_t size) {
	int i;

	table->systems = (EntitySystemEntry*)calloc(size, sizeof(EntitySystemEntry));

	if (table->systems == NULL) {
		allocationError("table->systems");
		return CERROR;
	}

	table->size = size;

	// Fill with emtpy entry.
	for (i = 0; i < table->size; ++i)
		table->systems[i] = EMPTY_SYSTEM_ENTRY;

	return CSUCCESS;
}

ErrorCodes resizeEntitySystemTable(EntitySystemTable * table, size_t size) {
	int i;

	// Create if null.
	if (table->systems == NULL)
		return createEntitySystemTable(table, size);

	table->systems = (EntitySystemEntry*)reallocarray(table->systems, size, sizeof(EntitySystemEntry));

	if (table->systems == NULL) {
		allocationError("table->systems");
		return CERROR;
	}

	// Add empty entries.
	if (size > table->size)
		for (i = table->size; i < size; ++i)
			table->systems[i] = EMPTY_SYSTEM_ENTRY;

	table->size = size;
	return CSUCCESS;
};

ErrorCodes addSystemEntryToTable(EntitySystemTable * table, EntitySystemEntry entry, SystemId  * id) {
	ErrorCodes res = CSUCCESS;

	if (table->systems == NULL)
		table->size = 0;

	// Resize.
	res = resizeEntitySystemTable(table, table->size + 1);

	if (res != CSUCCESS)
		return res;

	// Set id.
	if (id != NULL)
		*id = table->size - 1;

	table->systems[table->size - 1] = entry;
	return res;
}

EntitySystemEntry getSystemEntryFromTable(EntitySystemTable table, SystemId id) {
	if (table.systems == NULL || id >= table.size)
		return EMPTY_SYSTEM_ENTRY;

	return table.systems[id];
}

EntitySystem getSystemFromTable(EntitySystemTable table, SystemId id) {
	if (table.systems == NULL || id >= table.size)
		return (EntitySystem){NULL, 0};

	return (EntitySystem){table.systems[id].system, id, NULL};
}

void closeEntitySystemTable(EntitySystemTable * table) {
	if (table->systems != NULL)
		free(table->systems);

	table->systems = NULL;
	table->size = 0;

	TraceLog(LOG_INFO, "Entity system table closed");
}
