// C headers.
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <time.h>

// Raylib.
#include <raylib.h>
#include <raymath.h>
#include "raygui.h"

#ifndef GAME_DATA_H
#define GAME_DATA_H

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

#define TILE_SIZE 16
#define CHUNK_SIZE 5

typedef enum ErrorCodes {
	CSUCCESS = 0,
	CERROR = -1
} ErrorCodes;

// Types be like.
typedef struct GameData GameData;
typedef struct World World;

#endif
