#include "initFileLoader.h"
#include "sound.h"

const InitFileData defaultInitFileData = {
	.jukeBoxMode = JUKEBOX_NORMAL
};

void initConfigFileList(ConfigFileList * fileList) {
	fileList->listStr[0] = '\0';
	fileList->files = NULL;
	fileList->filesSize = 0;
}

void closeConfigFileList(ConfigFileList * fileList) {
	if (fileList->files != NULL)
		freeSplitedString(fileList->files, fileList->filesSize);

	fileList->files = NULL;
	fileList->filesSize = 0;
}

void getFileList(ConfigFileList * fileList) {

	// Free.
	closeConfigFileList(fileList);

	fileList->files = splitString(
		fileList->listStr,
		FILE_LIST_MAX,
		&fileList->filesSize,
		' '
	);
}

void initInitFileData(InitFileData * initData) {
	const ConfigLine configLines[] = {
		{"jukeBoxMode", &initData->jukeBoxMode, VALUE_INT}
	};

	// Copy default over.
	memcpy(initData, &defaultInitFileData, sizeof(InitFileData));

	initData->configLinesSize = sizeof(configLines) / sizeof(ConfigLine);
	initData->configLines = allocateConfigLines(configLines, initData->configLinesSize);
	initConfigLines(initData->configLines, initData->configLinesSize);

	TraceLog(LOG_INFO, "Init file data created");
}

void closeInitFileData(InitFileData * initData) {
	if (initData->configLines != NULL)
		free(initData->configLines);

	TraceLog(LOG_INFO, "Init file data closed");
}

ConfigErrors loadInitFileDataFromBuf(InitFileData * initData, const char * buf, size_t bufSize) {
	return loadConfigFromBuf(
		buf,
		bufSize,
		initData->configLines,
		initData->configLinesSize
	);
}
