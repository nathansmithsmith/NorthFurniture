#include "physics.h"
#include "util.h"
#include <chipmunk/chipmunk_private.h>

PhysicsComponent * createPhysicsComponent() {

	// Allocate physics component.
	PhysicsComponent * component = (PhysicsComponent*)malloc(sizeof(PhysicsComponent));

	if (component == NULL) {
		allocationError("PhysicsComponent");
		return NULL;
	}

	component->body = NULL;
	component->usingStaticBody = false;
	component->shapes = NULL;
	component->shapesCount = 0;

	component->velocityCb = DEFAULT_PHYSICS_CB;
	component->positionCb = DEFAULT_PHYSICS_CB;

	return component;
}

void freePhysicsComponent(PhysicsComponent * component) {
	int i;

	// Free shapes.
	if (component->shapes != NULL) {
		for (i = 0; i < component->shapesCount; ++i)
			if (component->shapes[i] != NULL)
				cpShapeFree(component->shapes[i]);

		free(component->shapes);
	}

	// Free body.
	if (component->body != NULL && !component->usingStaticBody)
		cpBodyFree(component->body);

	// Free everything else.
	free(component);
}

ErrorCodes setShapesInPhysicsComponent(PhysicsComponent * component, const cpShape ** shapes, size_t shapesCount) {
	if (component->body == NULL)
		return CERROR;

	// Allocate shapes.
	component->shapes = (cpShape**)calloc(shapesCount, sizeof(cpShape*));

	if (component->shapes == NULL) {
		allocationError("component->shapes");
		return CERROR;
	}

	component->shapesCount = shapesCount;

	// Copy shapes over.
	memcpy(component->shapes, shapes, shapesCount * sizeof(cpShape*));

	return CSUCCESS;
}

cpBodyVelocityFunc setVelocityCb(PhysicsComponent * component, int id) {
	if (id >= VELOCITY_UPDATE_CBS_SIZE || component->body == NULL)
		return NULL;

	cpBodyVelocityFunc func;

	if (id == DEFAULT_PHYSICS_CB)
		func = cpBodyUpdateVelocity;
	else
		func = velocityUpdateCbs[id].func;

	cpBodySetVelocityUpdateFunc(component->body, func);
	component->velocityCb = id;
	return func;
}

cpBodyPositionFunc setPositionCb(PhysicsComponent * component, int id) {
	if (id >= POSITION_UPDATE_CBS_SIZE || component->body == NULL)
		return NULL;

	cpBodyPositionFunc func;

	if (id == DEFAULT_PHYSICS_CB)
		func = cpBodyUpdatePosition;
	else
		func = positionUpdateCbs[id].func;

	cpBodySetPositionUpdateFunc(component->body, func);
	component->positionCb = id;
	return func;
}

const VelocityFuncEntry velocityUpdateCbs[VELOCITY_UPDATE_CBS_SIZE] = {
	(VelocityFuncEntry){"No Angle", updateVelocityNoAngle}
};


const PositionFuncEntry positionUpdateCbs[POSITION_UPDATE_CBS_SIZE] = {
	(PositionFuncEntry){"No Angle", updatePositionNoAngle}
};

// Copy and pasted this fucker (:
static void SetTransform(cpBody *body, cpVect p, cpFloat a) {
	cpVect rot = cpvforangle(a);
	cpVect c = body->cog;
	
	body->transform = cpTransformNewTranspose(
		rot.x, -rot.y, p.x - (c.x*rot.x - c.y*rot.y),
		rot.y,  rot.x, p.y - (c.x*rot.y + c.y*rot.x)
	);
}

void updatePositionNoAngle(cpBody * body, cpFloat dt) {
	cpVect p = body->p = cpvadd(body->p, cpvmult(cpvadd(body->v, body->v_bias), dt));
	SetTransform(body, p, 0);
	
	body->v_bias = cpvzero;
	body->w_bias = 0.0f;
}

void updateVelocityNoAngle(cpBody * body, cpVect gravity, cpFloat damping, cpFloat dt) {
	// Skip kinematic bodies.
	if(cpBodyGetType(body) == CP_BODY_TYPE_KINEMATIC) return;
	
	cpAssertSoft(body->m > 0.0f && body->i > 0.0f, "Body's mass and moment must be positive to simulate. (Mass: %f Moment: %f)", body->m, body->i);
	
	body->v = cpvadd(cpvmult(body->v, damping), cpvmult(cpvadd(gravity, cpvmult(body->f, body->m_inv)), dt));

	body->w = 0;
	
	// Reset forces.
	body->f = cpvzero;
	body->t = 0.0f;
}

Color toRayColor(cpSpaceDebugColor color) {
	return (Color){color.r, color.g, color.b, color.a};
}

Vector2 toRayVector2(cpVect vect) {
	return (Vector2){vect.x, vect.y};
}

void debugDrawCircle(cpVect p, cpFloat a, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data) {
	DrawCircle(p.x, p.y, r, toRayColor(fill));
	DrawCircleLines(p.x, p.y, r, toRayColor(outline));
}

void debugDrawSegment(cpVect a, cpVect b, cpSpaceDebugColor color, cpDataPointer data) {
	DrawLine(a.x, a.y, b.x, b.y, toRayColor(color));
}

void debugDrawFatSegment(cpVect a, cpVect b, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data) {
	Vector2 startPos = toRayVector2(a);
	Vector2 endPos = toRayVector2(b);

	// Yes we draw outline first. weird indeed

	// Draw outline.
	DrawLineEx(startPos, endPos, r, toRayColor(outline));

	// Draw fill.
	DrawLineEx(startPos, endPos, r - 2.0, toRayColor(fill));
}

// Used for lines in polygon when drawing.
typedef struct PolyLine {
	Vector2 start;
	Vector2 end;
} PolyLine;

// TODO: Better polygon filling.
void debugDrawPolygon(int count, const cpVect *verts, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data) {
	int i;
	int x, y;

	Vector2 start, end;
	Vector2 minVect, maxVect;
	Color outlineColor = toRayColor(outline);
	Color fillColor = toRayColor(fill);

	size_t pointsSize = count + 1;
	Vector2 points[pointsSize]; // Used for filling in.
	
	minVect = toRayVector2(verts[0]);
	maxVect = minVect;

	// Get min and max values and points.
	for (i = 0; i < count; ++i) {
		start = toRayVector2(verts[i]);
		points[i] = start;

		minVect.x = fmin(minVect.x, start.x);
		minVect.y = fmin(minVect.y, start.y);
		maxVect.x = fmax(maxVect.x, start.x);
		maxVect.y = fmax(maxVect.y, start.y);
	}

	points[pointsSize - 1] = points[0]; // Last point for CheckCollisionPointPoly.

	// Fillin.
	for (y = minVect.y; y < maxVect.y; ++y)
		for (x = minVect.x; x < maxVect.x; ++x)
			if (CheckCollisionPointPoly((Vector2){x, y}, points, pointsSize))
				DrawPixel(x, y, fillColor);

	// Draw outline.
	for (i = 0; i < count; ++i) {
		start = toRayVector2(verts[i]);

		if (i >= count - 1)
			end = toRayVector2(verts[0]);
		else
			end = toRayVector2(verts[i + 1]);

		DrawLineV(start, end, outlineColor);
	}
}

void debugDrawDot(cpFloat size, cpVect pos, cpSpaceDebugColor color, cpDataPointer data) {
	DrawCircle(pos.x, pos.y, size / 2.0, toRayColor(color));
}

cpSpaceDebugColor debugColorForShape(cpShape *shape, cpDataPointer data) {
	return (cpSpaceDebugColor){255.0, 0.0, 0.0, 50.0};
}

void drawPhysicsSpace(cpSpace * space) {
	cpSpaceDebugDrawOptions drawOptions = {
		.drawCircle = debugDrawCircle,
		.drawSegment = debugDrawSegment,
		.drawFatSegment = debugDrawFatSegment,
		.drawPolygon = debugDrawPolygon,
		.drawDot = debugDrawDot,
		.flags = (cpSpaceDebugDrawFlags)(CP_SPACE_DEBUG_DRAW_SHAPES | CP_SPACE_DEBUG_DRAW_CONSTRAINTS | CP_SPACE_DEBUG_DRAW_COLLISION_POINTS),
		.colorForShape = debugColorForShape,
		.shapeOutlineColor = (cpSpaceDebugColor){255.0, 255.0, 0.0, 255.0},
		.collisionPointColor = (cpSpaceDebugColor){0.0, 0.0, 255.0, 255.0},
		.data = NULL
	};

	cpSpaceDebugDraw(space, &drawOptions);
}
