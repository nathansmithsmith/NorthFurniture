#include "gameData.h"
#include "ecs.h"

#ifndef ENTITY_MAP_H
#define ENTITY_MAP_H

typedef struct EntityList {
	EntityId * ids;
	size_t count;
} EntityList;

typedef struct EntityList2d {
	EntityId * ids;
	int width;
	int height;
} EntityList2d;

// Stores ids for entitys in game.
typedef struct EntityMap {
	EntityList alive;
	EntityList dead;
} EntityMap;

void initEntityMap(EntityMap * entityMap);
void closeEntityMap(EntityMap * entityMap);

// Entity list.
ErrorCodes createEntityList(EntityList * entityList, size_t size);
void freeEntityList(EntityList * entityList);

// Resize list or create list.
ErrorCodes resizeEntityList(EntityList * entityList, size_t size);

// Add id to end of list and create list if not created.
ErrorCodes addEntityToList(EntityList * entityList, EntityId id);

// Sets item in index to id.
ErrorCodes setEntityInList(EntityList * entityList, int index, EntityId id);

// Gets item at index.
EntityId getEntityFromList(EntityList entityList, int index);

// Removes last item in list.
ErrorCodes popBackEntityList(EntityList * entityList);

// Remove item at index.
ErrorCodes removeEntityFromListIndex(EntityList * entityList, int index);

// Remove item with id.
ErrorCodes removeEntityFromListId(EntityList * entityList, EntityId id);

// run all the systems in the entity list.
void updateEntityList(GameData * gameData, World * world, EntityList entityList);

// Entity list 2d.
ErrorCodes createEntityList2d(EntityList2d * entityList, int width, int height);
void freeEntityList2d(EntityList2d * entityList);

// Sets item at position to id.
ErrorCodes setEntityInList2d(EntityList2d * entityList, Vector2 position, EntityId id);

// Gets item at position.
EntityId getEntityFromList2d(EntityList2d entityList, Vector2 position);

// Runs systems in area in entity list.
void updateEntityListArea2d(GameData * gameData, World * world, EntityList2d entityList, Rectangle area);

// Runs systems in entity list.
void updateEntityList2d(GameData * gameData, World * world, EntityList2d entityList);

#endif
