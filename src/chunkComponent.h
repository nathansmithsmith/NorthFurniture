#include "gameData.h"

#ifndef CHUNK_COMPONENT_H
#define CHUNK_COMPONENT_H

#define CHUNK_TILE_COUNT (int)(CHUNK_SIZE * CHUNK_SIZE)
#define CHUNK_PIXEL_SIZE (int)(CHUNK_SIZE * TILE_SIZE)
#define CHUNK_SCREEN_WIDTH (int)(SCREEN_WIDTH / CHUNK_PIXEL_SIZE)
#define CHUNK_SCREEN_HEIGHT (int)(SCREEN_HEIGHT / CHUNK_PIXEL_SIZE)

typedef uint16_t ChunkInt;

// Extra thicc and chunky.
typedef struct ChunkComponent {
	int textureId;
	ChunkInt positions[CHUNK_TILE_COUNT]; // Positions for each tile in sprite sheet.
	// Unit tile.
	// position = y * width + x
} ChunkComponent;

ChunkComponent * createChunkComponent();
void freeChunkComponent(ChunkComponent * component);

#endif
