#include "gameData.h"
#include "ecs.h"

#ifndef ENTITY_SYSTEMS_H
#define ENTITY_SYSTEMS_H

// Systems.
void drawSystem(SystemArgs args);
void drawSystemRect(SystemArgs args);
void drawChunkSystem(SystemArgs args);
void testSystem(SystemArgs args);
void physicsSystem(SystemArgs args);

// Ids for default systems.
enum {
	DRAW_SYSTEM,
	DRAW_SYSTEM_RECT,
	DRAW_CHUNK_SYSTEM,
	TEST_STSTEM,
	PHYSICS_SYSTEM
};

#define SYSTEM_ENTRY_NAME_MAX 25

typedef struct EntitySystemEntry {
	char name[SYSTEM_ENTRY_NAME_MAX];
	SYSTEM_CB system;
} EntitySystemEntry;

#define EMPTY_SYSTEM_ENTRY (EntitySystemEntry){"NULL", NULL}

// Look up table for every system.
typedef struct EntitySystemTable {
	size_t size;
	EntitySystemEntry * systems;
} EntitySystemTable;

// Allocates memory and adds default systems.
ErrorCodes initEntitySystemTable(EntitySystemTable * table);

// Allocate memory for table.
ErrorCodes createEntitySystemTable(EntitySystemTable * table, size_t size);

// Resizes or creates if null.
ErrorCodes resizeEntitySystemTable(EntitySystemTable * table, size_t size);

// Adds system to table and sets id if not null.
ErrorCodes addSystemEntryToTable(EntitySystemTable * table, EntitySystemEntry entry, SystemId  * id);

// Returns system at id.
EntitySystemEntry getSystemEntryFromTable(EntitySystemTable table, SystemId id);
EntitySystem getSystemFromTable(EntitySystemTable table, SystemId id);

void closeEntitySystemTable(EntitySystemTable * table);

#endif
