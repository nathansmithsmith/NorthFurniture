#include "componentToFromBuffer.h"
#include "game.h"
#include "textureLoader.h"
#include "util.h"
#include "physicsComponentBuffer.h"
#include "chunkComponent.h"

// Position.
void * toBufferPosition(ToBufferArgs args) {
	Vector2 * position = args.components.position;
	size_t dataSize = sizeof(Vector2);
	void * data = malloc(dataSize);

	if (data == NULL) {
		allocationError("Position component to buffer");
		return NULL;
	}

	memcpy(data, position, dataSize);
	*args.dataSize = dataSize;
	return data;
}

void fromBufferPosition(FromBufferArgs args) {
	Vector2 pos = *((Vector2*)args.data);
	args.components->position->x = pos.x;
	args.components->position->y = pos.y;
}

// Angle.
void * toBufferAngle(ToBufferArgs args) {
	float angle = args.components.angle;
	size_t dataSize = sizeof(float);
	void * data = malloc(dataSize);

	if (data == NULL) {
		allocationError("Float component to buffer");
		return NULL;
	}

	memcpy(data, &angle, dataSize);
	*args.dataSize = dataSize;
	return data;
}

void fromBufferAngle(FromBufferArgs args) {
	args.components->angle = *((float*)args.data);
}

// Texture.
void * toBufferTexture(ToBufferArgs args) {
	int32_t id = args.components.texture->id;
	size_t dataSize = sizeof(int32_t);
	void * data = malloc(dataSize);

	if (data == NULL) {
		allocationError("Texture component to buffer");
		return NULL;
	}

	memcpy(data, &id, dataSize);
	*args.dataSize = dataSize;
	return data;
}

void fromBufferTexture(FromBufferArgs args) {
	int32_t id = *((int32_t*)args.data);

	setTextureComponent(
		args.components->texture,
		args.gameData->textureData,
		id
	);
}

// Rect.
void * toBufferRect(ToBufferArgs args) {
	Rectangle * rect = args.components.rect;
	size_t dataSize = sizeof(Rectangle);
	void * data = malloc(dataSize);

	if (data == NULL) {
		allocationError("Rectangle component to buffer");
		return NULL;
	}

	memcpy(data, rect, dataSize);
	*args.dataSize = dataSize;
	return data;
}

void fromBufferRect(FromBufferArgs args) {
	Rectangle rect = *((Rectangle*)args.data);
	args.components->rect->x = rect.x;
	args.components->rect->y = rect.y;
	args.components->rect->width = rect.width;
	args.components->rect->height = rect.height;
}

// Chunk.
void * toBufferChunk(ToBufferArgs args) {
	int i;
	ChunkComponent * chunk = args.components.chunk;
	size_t dataSize = (sizeof(ChunkInt) * CHUNK_TILE_COUNT) + sizeof(int32_t);
	void * data = malloc(dataSize);
	void * current = data;

	if (data == NULL) {
		allocationError("Chunk component to buffer");
		return NULL;
	}

	// Write texture id.
	int32_t textureId = (int32_t)chunk->textureId;
	current = writeToBuffer(current, &textureId, sizeof(int32_t));

	ChunkInt pos;

	// Write positions.
	for (i = 0; i < CHUNK_TILE_COUNT; ++i) {
		pos = (ChunkInt)chunk->positions[i];
		current = writeToBuffer(current, &pos, sizeof(ChunkInt));
	}

	// Check current.
	if ((data + dataSize) - current != 0)
		TraceLog(LOG_ERROR, "Buffer wrong size when creating chunk component buffer");

	*args.dataSize = dataSize;
	return data;
}

void fromBufferChunk(FromBufferArgs args) {
	int i;
	const void * current = args.data;

	// Check size.
	if (args.dataSize != (sizeof(ChunkInt) * CHUNK_TILE_COUNT) + sizeof(int32_t)) {
		TraceLog(LOG_ERROR, "Chunk component buffer wrong size when loading");
		return;
	}

	// Load texture.
	int32_t textureId;
	current = loadFromBuffer(&textureId, current, sizeof(int32_t));
	args.components->chunk->textureId = (int)textureId;

	ChunkInt pos;

	// Load positions.
	for (i = 0; i < CHUNK_TILE_COUNT; ++i) {
		current = loadFromBuffer(&pos, current, sizeof(ChunkInt));
		args.components->chunk->positions[i] = (ChunkInt)pos;
	}

	// Check current.
	if ((args.data + args.dataSize) - current != 0)
		TraceLog(LOG_ERROR, "Buffer wrong size when loading chunk component buffer");
}

// Use same order as entity signatures enum.
const ComponentToFromBuffer ToFromBufferTable[COMPONENT_COUNT] = {
	{toBufferPosition, fromBufferPosition},
	{toBufferAngle, fromBufferAngle},
	{toBufferTexture, fromBufferTexture},
	{toBufferRect, fromBufferRect},
	{toBufferPhysics, fromBufferPhysics},
	{toBufferChunk, fromBufferChunk}
};

ComponentDataBuffer createComponentBuffer(GameData * gameData, EntityComponents components, EntitySignature signature) {
	int i;
	ComponentDataBuffer buffer;
	ComponentDataBufferNode * currNode = &buffer.node;
	EntitySignature currSign;

	// Set 'buffer'.
	buffer = (ComponentDataBuffer){
		.signature = signature,
		.nodeCount = 0,
		.bufferSize = 0
	};

	// What to return on errors.
	ComponentDataBuffer nullBuffer = (ComponentDataBuffer){
		.signature = NONE_COMPONENT,
		.nodeCount = 0,
		.node = (ComponentDataBufferNode){0, NULL, NULL}
	};

	// Set 'args'.
	ToBufferArgs args = (ToBufferArgs){
		.gameData = gameData,
		.components = components,
		.signature = signature
	};

	for (i = 0; i < COMPONENT_COUNT; ++i) {
		currSign = 0x1 << i;

		// Does have 'currSign'.
		if (!HAS_FLAG(signature, currSign))
			continue;

		// Get component data buffer.
		args.dataSize = &currNode->dataSize;
		currNode->data = ToFromBufferTable[i].toCb(args);

		++buffer.nodeCount;
		buffer.bufferSize += currNode->dataSize;

		// END END END!!!!
		if ((signature >> i) == 0x1) {
			currNode->next = NULL;
			break;
		}

		// Allocate 'next'.
		currNode->next = (ComponentDataBufferNode*)malloc(sizeof(ComponentDataBufferNode));

		if (currNode->next == NULL) {
			allocationError("currNode->next");
			return nullBuffer;
		}

		currNode = currNode->next;
	}

	return buffer;
}

ErrorCodes writeBufferToComponent(ComponentDataBuffer buffer, GameData * gameData, World * world, EntityComponents * components) {
	int i;
	ComponentDataBufferNode * currNode = &buffer.node;
	EntitySignature currSign;

	FromBufferArgs args = (FromBufferArgs){
		.gameData = gameData,
		.components = components,
		.world = world
	};

	for (i = 0; i < COMPONENT_COUNT; ++i) {
		currSign = 0x1 << i;

		// Check signature.
		if (!HAS_FLAG(buffer.signature, currSign))
			continue;

		args.dataSize = currNode->dataSize;
		args.signature = buffer.signature;
		args.data = currNode->data;
		ToFromBufferTable[i].fromCb(args);

		// END END END!!!!
		if ((buffer.signature >> i) == 0x1)
			break;

		currNode = currNode->next;

		if (currNode == NULL)
			break;
	}

	return CSUCCESS;
}

void freeComponentBuffer(ComponentDataBuffer * buffer) {
	int i;
	ComponentDataBufferNode * node = &buffer->node;
	ComponentDataBufferNode * lastNode = NULL;

	// Free nodes.
	for (i = 0; i < buffer->nodeCount; ++i) {
		if (node == NULL)
			break;

		// Free data in node.
		if (node->data != NULL)
			free(node->data);

		// Next node.
		lastNode = node;
		node = node->next;

		// Free node if not first node.
		if (i >= 1)
			free(lastNode);
	}

	buffer->signature = NONE_COMPONENT;
	buffer->nodeCount = 0;
	buffer->node = (ComponentDataBufferNode){0, NULL, NULL};
}

ComponentDataBufferList createComponentDataBufferList(EcsData ecsData, GameData * gameData) {
	int i;
	ComponentDataBufferListNode * node;
	EntityComponents * components;
	EntitySignature signature;

	ComponentDataBufferList bufferList = (ComponentDataBufferList){
		.nodeCount = ecsData.entityCount,
		.buffersSize = 0,
		.componentsCount = 0,
		.node.next = NULL
	};

	node = &bufferList.node;

	ComponentDataBufferList nullList = (ComponentDataBufferList){
		.nodeCount = 0,
		.buffersSize = 0
	};

	for (i = 0; i < bufferList.nodeCount; ++i) {
		components = getComponents(ecsData, i);
		signature = getSignature(ecsData, i);

		if (components == NULL)
			return nullList;

		// Create buffer.
		node->buffer = createComponentBuffer(
			gameData,
			*components,
			signature
		);

		bufferList.buffersSize += node->buffer.bufferSize;
		bufferList.componentsCount += node->buffer.nodeCount;

		// That's all folks
		if (i == bufferList.nodeCount - 1) {
			node->next = NULL;
			break;
		}

		// Allocate next.
		node->next = (ComponentDataBufferListNode*)malloc(sizeof(ComponentDataBufferListNode));

		if (node->next == NULL) {
			allocationError("node->next");
			return nullList;
		}

		node = node->next;
	}

	return bufferList;
}

void freeComponentDataBufferList(ComponentDataBufferList * bufferList) {
	int i;
	ComponentDataBufferListNode * node;
	ComponentDataBufferListNode * lastNode = NULL;

	if (bufferList->nodeCount <= 0)
		return;

	node = &bufferList->node;

	// I leave no comment to make you suffer!!!

	for (i = 0; i < bufferList->nodeCount; ++i) {
		if (node == NULL)
			break;

		freeComponentBuffer(&node->buffer);

		lastNode = node;
		node = node->next;

		if (i >= 1)
			free(lastNode);
	}

	bufferList->nodeCount = 0;
	bufferList->buffersSize = 0;
}
