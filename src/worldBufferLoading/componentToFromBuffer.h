#include "gameData.h"
#include "components.h"
#include "ecs.h"
#include "world.h"

#ifndef COMPONENT_TO_FROM_BUFFER_H
#define COMPONENT_TO_FROM_BUFFER_H

// Used for storing components in files.
typedef struct ComponentDataBufferNode {
	size_t dataSize;
	void * data;
	struct ComponentDataBufferNode * next;
} ComponentDataBufferNode;

typedef struct ComponentDataBuffer {
	EntitySignature signature;
	size_t nodeCount;
	size_t bufferSize;
	ComponentDataBufferNode node;
} ComponentDataBuffer;

// List of all the components data buffer in a linked list.
typedef struct ComponentDataBufferListNode {
	ComponentDataBuffer buffer;
	struct ComponentDataBufferListNode * next;
} ComponentDataBufferListNode;

typedef struct ComponentDataBufferList {
	size_t nodeCount;
	size_t buffersSize;
	size_t componentsCount;
	ComponentDataBufferListNode node;
} ComponentDataBufferList;

typedef struct ToBufferArgs {
	GameData * gameData;
	EntityComponents components;
	EntitySignature signature;
	size_t * dataSize;
} ToBufferArgs;

// Trump is a lizard person.
typedef struct FromBufferArgs {
	GameData * gameData;
	EntityComponents * components;
	EntitySignature signature;
	size_t dataSize;
	const void * data;
	World * world;
} FromBufferArgs;

typedef void * (*TO_BUFFER_CB)(ToBufferArgs args); // void * needs to be freed after use.
typedef void (*FROM_BUFFER_CB)(FromBufferArgs args);

// Item in a table of 
typedef struct ComponentToFromBuffer {
	TO_BUFFER_CB toCb;
	FROM_BUFFER_CB fromCb;
} ComponentToFromBuffer;

extern const ComponentToFromBuffer ToFromBufferTable[COMPONENT_COUNT];

ComponentDataBuffer createComponentBuffer(GameData * gameData, EntityComponents components, EntitySignature signature);
ErrorCodes writeBufferToComponent(ComponentDataBuffer buffer, GameData * gameData, World * world, EntityComponents * components);
void freeComponentBuffer(ComponentDataBuffer * buffer);

ComponentDataBufferList createComponentDataBufferList(EcsData ecsData, GameData * gameData);
void freeComponentDataBufferList(ComponentDataBufferList * bufferList);

#endif
