#include "worldBuffer.h"
#include "componentToFromBuffer.h"
#include "entitySystems.h"
#include "components.h"
#include "game.h"
#include "util.h"

WorldCheckSum runWorldCheckSum(WorldBuffer buffer) {
	int i;
	WorldCheckSum checksum = 0;
	WorldCheckSum * dataChuncks = (WorldCheckSum*)(buffer.data + sizeof(WorldHeader));

	size_t checkSumSize = buffer.size - sizeof(WorldHeader);
	int chuncks = checkSumSize / sizeof(WorldCheckSum);
	int bounsBytes = checkSumSize % sizeof(WorldCheckSum);

	// Checksum chuncks.
	for (i = 0; i < chuncks; ++i)
		checksum ^= dataChuncks[i];

	// Bouns bytes.
	for (i = buffer.size - bounsBytes; i < buffer.size; ++i)
		checksum += *((uint8_t*)buffer.data + i);

	return checksum;
}

size_t getEntityListBufferSize(EntityList entityList) {
	return sizeof(WorldUInt) + (entityList.count * sizeof(EntityId));
}

size_t getEntityList2dBufferSize(EntityList2d entityList) {
	return (sizeof(WorldUInt) * 2) + (entityList.width * entityList.height * sizeof(EntityId));
}

void * writeEntityList(void * data, EntityList entityList) {
	void * current = data;
	WorldUInt count = (WorldUInt)entityList.count;

	// Write size.
	current = writeToBuffer(current, &count, sizeof(WorldUInt));

	// Write entities.
	current = writeToBuffer(current, entityList.ids, entityList.count * sizeof(EntityId));

	return current;
}

void * loadEntityList(void * data, EntityList * entityList) {
	void * current = data;
	WorldUInt count;

	// Load count.
	current = loadFromBuffer(&count, current, sizeof(WorldUInt));

	// Resize and load ids.
	resizeEntityList(entityList, count);
	current = loadFromBuffer(entityList->ids, current, sizeof(EntityId) * count);

	return current;
}

void * writeEntityList2d(void * data, EntityList2d entityList) {
	void * current = data;
	WorldUInt width = (WorldUInt)entityList.width;
	WorldUInt height = (WorldUInt)entityList.height;

	// Write width and height.
	current = writeToBuffer(current, &width, sizeof(WorldUInt));
	current = writeToBuffer(current, &height, sizeof(WorldUInt));

	size_t idsSize = entityList.width * entityList.height * sizeof(EntityId);

	// Write list.
	current = writeToBuffer(current, entityList.ids, idsSize);

	return current;
}

void * loadEntityList2d(void * data, EntityList2d * entityList) {
	void * current = data;
	WorldUInt width, height;

	// Get width and height.
	current = loadFromBuffer(&width, current, sizeof(WorldUInt));
	current = loadFromBuffer(&height, current, sizeof(WorldUInt));

	// Resize.
	if (entityList->ids != NULL)
		freeEntityList2d(entityList);

	createEntityList2d(entityList, width, height);

	size_t size = width * height * sizeof(EntityId);

	// Load.
	current = loadFromBuffer(entityList->ids, current, size);

	return current;
}

size_t getChunkMapBufferSize(ChunkMap chunkMap) {
	size_t size = getEntityList2dBufferSize(chunkMap.chunks);
	return size;
}

void * writeChunkMap(void * data, ChunkMap chunkMap) {
	void * current = data;
	current = writeEntityList2d(current, chunkMap.chunks);
	return current;
}

void * loadChunkMap(void * data, ChunkMap * chunkMap) {
	void * current = data;
	WorldUInt width, height;

	// Get width and height.
	current = loadFromBuffer(&width, current, sizeof(WorldUInt));
	current = loadFromBuffer(&height, current, sizeof(WorldUInt));

	// Resize this fucker.
	freeChunkMap(chunkMap);
	*chunkMap = createChunkMap(width, height);

	size_t size = width * height * sizeof(EntityId);

	// Load.
	current = loadFromBuffer(chunkMap->chunks.ids, current, size);

	return current;
}

void * writeComponentDataBuffer(void * data, ComponentDataBuffer buffer) {
	int i;
	void * current = data;
	ComponentDataBufferNode * node = &buffer.node;
	WorldComponentSizeInt size = (WorldComponentSizeInt)buffer.bufferSize;

	// Write size.
	current = writeToBuffer(current, &size, sizeof(WorldComponentSizeInt));

	// Write buffer.
	for (i = 0; i < buffer.nodeCount; ++i) {
		size = (WorldComponentSizeInt)node->dataSize;

		current = writeToBuffer(current, &size, sizeof(WorldComponentSizeInt));

		// Write component buffer.
		if (node->data != NULL && node->dataSize != 0)
			current = writeToBuffer(current, node->data, node->dataSize);

		node = node->next;

		if (node == NULL)
			break;
	}

	return current;
}

void * writeSystemsToBuffer(void * data, EntitySystems systems) {
	int i;
	void * current = data;

	EntitySystem * node = &systems.system;
	SystemCountInt count = (SystemCountInt)systems.count;

	// Write count.
	current = writeToBuffer(current, &count, sizeof(SystemCountInt));

	// Write systems.
	for (i = 0; i < count; ++i) {
		current = writeToBuffer(current, &node->id, sizeof(SystemId));
		node = node->next;
	}

	return current;
}

WorldBuffer dumpWorldToBuf(GameData * gameData, World world) {
	int i;
	void * current;
	WorldBuffer buffer = (WorldBuffer){0, NULL};
	WorldOption options = WORLD_OPTIONS_NONE;

	ComponentDataBufferList components = createComponentDataBufferList(
		world.ecsData,
		gameData
	);

	ComponentDataBufferListNode * node = &components.node;

	WorldBuffer nullBuffer = (WorldBuffer){0, NULL};

	// Get size.
	buffer.size += sizeof(WorldHeader);
	buffer.size += sizeof(EntitySignature) * world.ecsData.entityCount;

	// Systems.
	buffer.size += world.ecsData.entityCount * sizeof(SystemCountInt);
	buffer.size += world.ecsData.systemCount * sizeof(SystemId);

	// Entity lists and chunk map.
	if (world.entityMap.alive.ids != NULL) {
		buffer.size += getEntityListBufferSize(world.entityMap.alive);
		options |= WORLD_OPTIONS_ALIVE_LIST;
	} if (world.entityMap.dead.ids != NULL) {
		buffer.size += getEntityListBufferSize(world.entityMap.dead);
		options |= WORLD_OPTIONS_DEAD_LIST;
	} if (world.chunkMap.chunks.ids != NULL) {
		buffer.size += getChunkMapBufferSize(world.chunkMap);
		options |= WORLD_OPTIONS_CHUNK_MAP;
	}

	// Components.
	size_t componentsSize = components.componentsCount * sizeof(WorldComponentSizeInt);
	componentsSize += components.nodeCount * sizeof(WorldComponentSizeInt);
	componentsSize += components.buffersSize;
	buffer.size += componentsSize;

	// Allocate buffer.
	buffer.data = malloc(buffer.size);

	if (buffer.data == NULL) {
		allocationError("buffer.data");
		return nullBuffer;
	}

	current = buffer.data;

	// Write header.
	WorldHeader header = (WorldHeader){
		.magicNumber = WORLD_HEADER_MAGIC_NUMBER,
		.entityCount = world.ecsData.entityCount,
		.componentsSize = componentsSize,
		.options = options,
		.checksum = 0
	};

	WorldHeader * headerPointer = (WorldHeader*)current;
	current = writeToBuffer(current, &header, sizeof(WorldHeader));

	// Write signatures.
	current = writeToBuffer(
		current, 
		world.ecsData.entitySignatures,
		world.ecsData.entityCount * sizeof(EntitySignature)
	);

	// Write systems.
	for (i = 0; i < world.ecsData.entityCount; ++i)
		current = writeSystemsToBuffer(current, *getSystems(world.ecsData, i));

	// Write entity lists and chunk map.
	if (world.entityMap.alive.ids != NULL)
		current = writeEntityList(current, world.entityMap.alive);
	if (world.entityMap.dead.ids != NULL)
		current = writeEntityList(current, world.entityMap.dead);
	if (world.chunkMap.chunks.ids != NULL)
		current = writeChunkMap(current, world.chunkMap);

	// Write components.
	for (i = 0; i < components.nodeCount; ++i) {
		current = writeComponentDataBuffer(current, node->buffer);
		node = node->next;

		if (node == NULL)
			break;
	}

	// free components.
	size_t componentCount = components.componentsCount;
	freeComponentDataBufferList(&components);

	// Check current..
	if ((buffer.data + buffer.size) - current != 0)
		TraceLog(LOG_ERROR, "World buffer wrong size when dumping");

	// Run checksum.
	((WorldHeader*)buffer.data)->checksum = runWorldCheckSum(buffer);

	// Log.
	TraceLog(
		LOG_INFO, 
		"Dumped world buffer with Entities: %ld, components: %ld",
		world.ecsData.entityCount,
		componentCount
	);
	
	return buffer;
}

void * loadComponentBuffer(void * data, ComponentDataBuffer * buffer, EntitySignature signature) {
	int i;
	void * current = data;
	EntitySignature currSign;
	WorldComponentSizeInt size;
	ComponentDataBufferNode * node = &buffer->node;

	// Load buffer size.
	current = loadFromBuffer(&size, current, sizeof(WorldComponentSizeInt));

	buffer->bufferSize = size;
	buffer->signature = signature;
	buffer->nodeCount = 0;

	for (i = 0; i < COMPONENT_COUNT; ++i) {
		currSign = 0x1 << i;

		// Does have 'currSign'.
		if (!HAS_FLAG(signature, currSign))
			continue;

		// Load size.
		current = loadFromBuffer(&size, current, sizeof(WorldComponentSizeInt));
		node->dataSize = size;

		// Allocate.
		node->data = malloc(node->dataSize);

		if (node->data == NULL) {
			allocationError("node->data");
			return current;
		}

		// Load.
		current = loadFromBuffer(node->data, current, node->dataSize);
		++buffer->nodeCount;

		// END END END!!!!
		if ((signature >> i) == 0x1) {
			node->next = NULL;
			break;
		}

		// Allocate 'next'.
		node->next = (ComponentDataBufferNode*)malloc(sizeof(ComponentDataBufferNode));

		if (node->next == NULL) {
			allocationError("node->next");
			return current;
		}

		node = node->next;
	}

	return current;
}

void * loadSystemsfromBuffer(void * data, EntityId id, EcsData * ecsData, EntitySystemTable table) {
	int i;
	void * current = data;
	SystemCountInt count;
	SystemId currId;

	// Load count.
	current = loadFromBuffer(&count, current, sizeof(SystemCountInt));

	if (count == 0)
		return current;

	EntitySystem systems[count];

	// Load systems.
	for (i = 0; i < count; ++i) {
		current = loadFromBuffer(&currId, current, sizeof(SystemId));
		systems[i] = getSystemFromTable(table, currId);
	}

	// Set systems.
	setEntitySystems(
		ecsData,
		id,
		systems,
		count
	);

	return current;
}

ErrorCodes loadWorld(World * worldOutput, GameData * gameData, WorldBuffer buffer) {
	int i;
	void * current = buffer.data;
	World world; WorldHeader header; EntityId currentEntity = 0; EntitySignature currSign;
	// Load header.
	current = loadFromBuffer(
		&header,
		current,
		sizeof(WorldHeader)
	);

	// Check magic number.
	if (header.magicNumber != WORLD_HEADER_MAGIC_NUMBER) {
		TraceLog(LOG_ERROR, "Incorrect magic number in world buffer");
		return CERROR;
	}

	// Checksum.
	if (runWorldCheckSum(buffer) != header.checksum) {
		TraceLog(LOG_ERROR, "Invalid checksum in world buffer");
		return CERROR;
	}

	// Create world.
	world = createWorld(header.entityCount);

	// Load signatures.
	current = loadFromBuffer(
		world.ecsData.entitySignatures,
		current,
		header.entityCount * sizeof(EntitySignature)
	);

	// Load systems.
	for (i = 0; i < header.entityCount; ++i)
		current = loadSystemsfromBuffer(current, i, &world.ecsData, gameData->systemTable);

	// Load entity map.
	if (HAS_FLAG(header.options, WORLD_OPTIONS_ALIVE_LIST))
		current = loadEntityList(current, &world.entityMap.alive);
	if (HAS_FLAG(header.options, WORLD_OPTIONS_DEAD_LIST))
		current = loadEntityList(current, &world.entityMap.dead);
	if (HAS_FLAG(header.options, WORLD_OPTIONS_CHUNK_MAP))
		current = loadChunkMap(current, &world.chunkMap);

	ComponentDataBuffer componentBuffer;
	ErrorCodes res;

	// Load components.
	for (i = 0; i < header.entityCount; ++i) {
		currSign = getSignature(world.ecsData, i);

		// Get buffer.
		current = loadComponentBuffer(current, &componentBuffer, currSign);

		setEntityComponents(world.ecsData, i, currSign);

		// Write to component.
		res = writeBufferToComponent(
			componentBuffer,
			gameData,
			&world,
			getComponents(world.ecsData, i)
		);

		if (res != CSUCCESS)
			TraceLog(LOG_WARNING, "Error loading entity %d", i);

		// Free.
		freeComponentBuffer(&componentBuffer);
	}

	if ((buffer.data + buffer.size) - current != 0)
		TraceLog(LOG_ERROR, "World buffer wrong size when loading");

	// Log.
	TraceLog(
		LOG_INFO,
		"Loaded world with Entities: %ld",
		world.ecsData.entityCount
	);

	*worldOutput = world;
	return CSUCCESS;
}

void freeWorldBuffer(WorldBuffer * buffer) {
	if (buffer->data != NULL)
		free(buffer->data);

	buffer->size = 0;
	buffer->data = NULL;
}
