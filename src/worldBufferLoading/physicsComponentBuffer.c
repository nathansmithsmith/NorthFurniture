#include "physicsComponentBuffer.h"
#include "util.h"

ErrorCodes setCircleShapeBuffer(PhysicsShapesBuffer * current, int id, PhysicsComponent component) {
	cpShape * shape = component.shapes[id];
	cpCircleShape * circle = (cpCircleShape*)shape;

	// Allocate.
	current->size = sizeof(PhysicsCircleBuffer);

	if ((current->buffer = malloc(current->size)) == NULL) {
		allocationError("current->buffer");
		return CERROR;
	}

	// Set values.
	*((PhysicsCircleBuffer*)current->buffer) = (PhysicsCircleBuffer){
		.offset = toBufferPhysicsVect(cpCircleShapeGetOffset(shape)),
		.r = cpCircleShapeGetRadius(shape)
	};

	return CSUCCESS;
}

ErrorCodes setSegmentShapeBuffer(PhysicsShapesBuffer * current, int id, PhysicsComponent component) {
	cpShape * shape = component.shapes[id];
	cpSegmentShape * segment = (cpSegmentShape*)shape;

	// Allocate.
	current->size = sizeof(PhysicsSegmentBuffer);

	if ((current->buffer = malloc(current->size)) == NULL) {
		allocationError("current->buffer");
		return CERROR;
	}

	// Set values.
	*((PhysicsSegmentBuffer*)current->buffer) = (PhysicsSegmentBuffer){
		.a = toBufferPhysicsVect(cpSegmentShapeGetA(shape)),
		.b = toBufferPhysicsVect(cpSegmentShapeGetB(shape)),
		.r = cpSegmentShapeGetRadius(shape)
	};

	return CSUCCESS;
}

// I wasted lots of time not wanting to do this.
ErrorCodes setPolyShapeBuffer(PhysicsShapesBuffer * current, int id, PhysicsComponent component) {
	int i;
	cpShape * shape = component.shapes[id];
	cpPolyShape * poly = (cpPolyShape*)poly;

	PhysicsPolyBufferHeader header = (PhysicsPolyBufferHeader){
		.vectCount = cpPolyShapeGetCount(shape),
		.r = cpPolyShapeGetRadius(shape)
	};

	// Allocate memory.
	current->size = sizeof(PhysicsPolyBufferHeader);
	current->size += sizeof(PhysicsBufferVect) * header.vectCount;

	if ((current->buffer = malloc(current->size)) == NULL) {
		allocationError("current->buffer");
		return CERROR;
	}

	void * data = current->buffer;

	// Write header.
	data = writeToBuffer(data, &header, sizeof(PhysicsPolyBufferHeader));

	cpVect v;

	// Write the rest of this fucker.
	for (i = 0; i < header.vectCount; ++i) {
		v = cpPolyShapeGetVert(shape, i);

		data = writeToBuffer(
			data, 
			&(PhysicsBufferVect){v.x, v.y},
			sizeof(PhysicsBufferVect)
		);
	}

	return CSUCCESS;
}

ErrorCodes setPhysicsShapeBuffer(PhysicsShapesBuffer * current, int id, PhysicsComponent component) {
	cpShape * shape = component.shapes[id];
	ErrorCodes res = CSUCCESS;

	*current = (PhysicsShapesBuffer){
		.buffer = NULL,
		.size = 0
	};

	current->type = shape->klass->type;

	switch (shape->klass->type) {
		case CP_CIRCLE_SHAPE:
			res = setCircleShapeBuffer(current, id, component);
			break;
		case CP_SEGMENT_SHAPE:
			res = setSegmentShapeBuffer(current, id, component);
			break;
		case CP_POLY_SHAPE:
			res = setPolyShapeBuffer(current, id, component);
			break;
		default:
			return CERROR;
	}

	return res;
}

cpShape * circleShapeFromBuffer(const void * data, const void ** dataNext, size_t size, cpBody * body) {
	const void * current = data;
	cpShape * shape = NULL;
	PhysicsCircleBuffer buffer;

	// Load buffer.
	current = loadFromBuffer(&buffer, current, sizeof(PhysicsCircleBuffer));

	// Create shape.
	shape = cpCircleShapeNew(body, buffer.r, fromBufferPhysicsVect(buffer.offset));

	*dataNext = current;
	return shape;
}

cpShape * segmentShapeFromBuffer(const void * data, const void ** dataNext, size_t size, cpBody * body) {
	const void * current = data;
	cpShape * shape = NULL;
	PhysicsSegmentBuffer buffer;

	// Load buffer.
	current = loadFromBuffer(&buffer, current, sizeof(PhysicsSegmentBuffer));

	// Create shape.
	shape = cpSegmentShapeNew(
		body,
		fromBufferPhysicsVect(buffer.a),
		fromBufferPhysicsVect(buffer.b),
		buffer.r
	);

	*dataNext = current;
	return shape;
}

cpShape * polyShapeFromBuffer(const void * data, const void ** dataNext, size_t size, cpBody * body) {
	int i;
	const void * current = data;
	cpShape * shape = NULL;
	PhysicsPolyBufferHeader header;

	// Load header.
	current = loadFromBuffer(&header, current, sizeof(PhysicsPolyBufferHeader));

	// Get verts.
	PhysicsBufferVect currVect;
	cpVect verts[header.vectCount];

	for (i = 0; i < header.vectCount; ++i) {
		current = loadFromBuffer(
			&currVect, 
			current, 
			sizeof(PhysicsBufferVect)
		);

		verts[i] = fromBufferPhysicsVect(currVect);
	}

	// Create shape.
	shape = cpPolyShapeNewRaw(body, header.vectCount, verts, header.r);

	*dataNext = current;
	return shape;
}

cpShape * shapeFromBuffer(const void * data, const void ** dataNext, cpBody * body) {
	const void * current = data;
	cpShape * shape = NULL;
	PhysicsShapeBufferHeader header;

	// Load header.
	current = loadFromBuffer(&header, current, sizeof(PhysicsShapeBufferHeader));

	// Load shape.
	switch (header.type) {
		case CP_CIRCLE_SHAPE:
			return circleShapeFromBuffer(current, dataNext, header.size, body);
		case CP_SEGMENT_SHAPE:
			return segmentShapeFromBuffer(current, dataNext, header.size, body);
		case CP_POLY_SHAPE:
			return polyShapeFromBuffer(current, dataNext, header.size, body);
		default:
			return NULL;
	}

	*dataNext = current;
	return shape;
}

ErrorCodes createPhysicsBuffer(PhysicsBuffer * buffer, PhysicsComponent component, EntitySignature signature) {
	int i;

	PhysicsBodyBuffer body = (PhysicsBodyBuffer){
		.type = cpBodyGetType(component.body),
		.options = PHYSICS_OPTION_NONE,
		.mass = cpBodyGetMass(component.body),
		.moment = cpBodyGetMoment(component.body),
		.position = toBufferPhysicsVect(cpBodyGetPosition(component.body)),
		.centerOfGravit = toBufferPhysicsVect(cpBodyGetCenterOfGravity(component.body)),
		.velocity = toBufferPhysicsVect(cpBodyGetVelocity(component.body)),
		.force = toBufferPhysicsVect(cpBodyGetForce(component.body)),
		.angle = cpBodyGetAngle(component.body),
		.angularVelocity = cpBodyGetAngularVelocity(component.body),
		.torque = cpBodyGetTorque(component.body),
		.velocityCb = component.velocityCb,
		.positionCb = component.positionCb
	};

	// Options.
	if (component.usingStaticBody)
		body.options |= PHYSICS_USING_STATIC_BODY;

	*buffer = (PhysicsBuffer){
		.body = body,
		.shapesSize = 0,
		.shapesCount = component.shapesCount,
	};

	PhysicsShapesBuffer * shape = &buffer->shapes;

	// Add shapes.
	for (i = 0; i < component.shapesCount; ++i) {
		ErrorCodes e = setPhysicsShapeBuffer(shape, i, component);

		if (e != CSUCCESS)
			return e;

		// Add to sum.
		buffer->shapesSize += shape->size;

		// At the fucking end.
		if (i == component.shapesCount - 1) {
			shape->next = NULL;
			break;
		}

		// Allocate next.
		shape->next = (PhysicsShapesBuffer*)malloc(sizeof(PhysicsShapesBuffer));

		if (shape->next == NULL) {
			allocationError("shape->next");
			return CERROR;
		}

		shape = shape->next;
	}

	return CSUCCESS;
}

void freePhysicsBuffer(PhysicsBuffer * buffer) {
	int i;

	// Free the shitty shapes.
	PhysicsShapesBuffer * shape = &buffer->shapes;
	PhysicsShapesBuffer * lastShape = NULL;

	for (i = 0; i < buffer->shapesCount; ++i) {
		if (shape == NULL)
			break;

		if (shape->buffer != NULL)
			free(shape->buffer);

		lastShape = shape;
		shape = shape->next;

		if (i >= 1)
			free(lastShape);
	}
}

cpBody * bodyFromBuffer(PhysicsBodyBuffer buffer) {
	cpBody * body = NULL;

	// Create body.
	switch (buffer.type) {
		case CP_BODY_TYPE_DYNAMIC:
			body = cpBodyNew(buffer.mass, buffer.moment);
			break;
		case CP_BODY_TYPE_KINEMATIC:
			body = cpBodyNewKinematic();
			break;
		case CP_BODY_TYPE_STATIC:
			body = cpBodyNewStatic();
			break;
		default:
			return NULL;
	}

	// Set lots and lots of shitty things.
	cpBodySetPosition(body, fromBufferPhysicsVect(buffer.position));
	cpBodySetCenterOfGravity(body, fromBufferPhysicsVect(buffer.centerOfGravit));
	cpBodySetVelocity(body, fromBufferPhysicsVect(buffer.velocity));
	cpBodySetForce(body, fromBufferPhysicsVect(buffer.force));
	
	cpBodySetAngle(body, buffer.angle);
	cpBodySetAngularVelocity(body, buffer.angularVelocity);
	cpBodySetTorque(body, buffer.torque);

	return body;
}

void * toBufferPhysics(ToBufferArgs args) {
	int i;

	if (args.components.physics->body == NULL) {
		*args.dataSize = 0;
		return NULL;
	}

	size_t dataSize = 0;
	void * data = NULL;
	void * current = NULL;

	PhysicsBuffer buffer;

	// Create buffer.
	if (createPhysicsBuffer(&buffer, *args.components.physics, args.signature) != CSUCCESS) {
		TraceLog(LOG_ERROR, "Error create physics buffer");
		return NULL;
	}

	// Get size.
	dataSize = sizeof(PhysicsBodyBuffer);
	dataSize += sizeof(PhysicsBufUInt); // Shapes count.
	dataSize += buffer.shapesSize;
	dataSize += buffer.shapesCount * sizeof(PhysicsShapeBufferHeader);

	// Allocate data.
	data = malloc(dataSize);

	if (data == NULL) {
		allocationError("Position component to buffer");
		freePhysicsBuffer(&buffer);
		return NULL;
	}

	current = data;

	// Write body.
	current = writeToBuffer(current, &buffer.body, sizeof(PhysicsBodyBuffer));
	PhysicsShapesBuffer * shape = &buffer.shapes;

	// Write shapes count.
	PhysicsBufUInt shapesCountBuf = (PhysicsBufUInt)buffer.shapesCount;
	current = writeToBuffer(current, &shapesCountBuf, sizeof(PhysicsBufUInt));

	// Write shapes.
	for (i = 0; i < buffer.shapesCount; ++i) {
		if (shape == NULL)
			break;

		// Write shapes header.
		current = writeToBuffer(
			current,
			&(PhysicsShapeBufferHeader){shape->type, shape->size},
			sizeof(PhysicsShapeBufferHeader)
		);

		// Write shape.
		current = writeToBuffer(current, shape->buffer, shape->size);

		shape = shape->next;
	}

	if (current - data != dataSize)
		TraceLog(LOG_ERROR, "Physics buffer wrong size when dumping");

	// Free buffer.
	freePhysicsBuffer(&buffer);

	*args.dataSize = dataSize;
	return data;
}

void fromBufferPhysics(FromBufferArgs args) {
	int i;

	if (args.dataSize <= 0)
		return;

	PhysicsBodyBuffer bodyBuf;
	const void * current = args.data;

	// Reallocate physics component.
	freePhysicsComponent(args.components->physics);
	args.components->physics = createPhysicsComponent();

	// Load body.
	current = loadFromBuffer(&bodyBuf, current, sizeof(PhysicsBodyBuffer));

	// Static body mode.
	if (HAS_FLAG(bodyBuf.options, PHYSICS_USING_STATIC_BODY)) {
		args.components->physics->body = cpSpaceGetStaticBody(args.world->physicsSpace);
		args.components->physics->usingStaticBody = true;
	} else {
		args.components->physics->body = bodyFromBuffer(bodyBuf);

		if (args.components->physics->body == NULL)
			return;

		cpSpaceAddBody(
			args.world->physicsSpace,
			args.components->physics->body
		);
	}

	// Add callbacks.
	setVelocityCb(args.components->physics, bodyBuf.velocityCb);
	setPositionCb(args.components->physics, bodyBuf.positionCb);

	// Load shapes count.
	PhysicsBufUInt shapesCount;
	current = loadFromBuffer(&shapesCount, current, sizeof(PhysicsBufUInt));

	if (shapesCount <= 0)
		return;

	// Load shapes.
	args.components->physics->shapesCount = shapesCount;
	args.components->physics->shapes = (cpShape**)calloc(shapesCount, sizeof(cpShape*));

	if (args.components->physics->shapes == NULL) {
		allocationError("args.components->physics->shapes");
		return;
	}

	for (i = 0; i < shapesCount; ++i) {
		args.components->physics->shapes[i] = shapeFromBuffer(
			current,
			&current,
			args.components->physics->body
		);

		cpSpaceAddShape(
			args.world->physicsSpace,
			args.components->physics->shapes[i]
		);
	}

	if (current - args.data != args.dataSize)
		TraceLog(LOG_ERROR, "Physics buffer wrong size when loading");
}
