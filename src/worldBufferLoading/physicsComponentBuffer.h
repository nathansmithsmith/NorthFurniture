#include "game.h"
#include "gameData.h"
#include "componentToFromBuffer.h"
#include "components.h"
#include "physics.h"
#include <chipmunk/chipmunk.h>
#include <chipmunk/chipmunk_structs.h>

#ifndef PHYSICS_COMPONENT_BUFFER_H
#define PHYSICS_COMPONENT_BUFFER_H

typedef uint32_t PhysicsOptions;
typedef int32_t PhysicsBufInt;
typedef uint32_t PhysicsBufUInt;

typedef struct PhysicsBufferVect {
	float x, y; // Simple indeed.
} PhysicsBufferVect;

static PhysicsBufferVect toBufferPhysicsVect(cpVect v) {
	return (PhysicsBufferVect){v.x, v.y};
}

static cpVect fromBufferPhysicsVect(PhysicsBufferVect v) {
	return cpv(v.x, v.y);
}

enum {
	PHYSICS_OPTION_NONE,
	PHYSICS_USING_STATIC_BODY = 0x1
};

typedef struct PhysicsBodyBuffer {
	uint32_t type;
	PhysicsOptions options;

	// A know you are a stupid peice of shit so I will remind you that floats are always 32 bit.
	float mass;
	float moment;

	PhysicsBufferVect position;
	PhysicsBufferVect centerOfGravit;
	PhysicsBufferVect velocity;
	PhysicsBufferVect force;

	float angle;
	float angularVelocity;
	float torque;

	// Callback ids.
	PhysicsBufInt velocityCb;
	PhysicsBufInt positionCb;

	uint32_t padding;
} PhysicsBodyBuffer;

typedef struct PhysicsShapesBuffer {
	cpShapeType type;
	void * buffer;
	size_t size;
	struct PhysicsShapesBuffer * next;
} PhysicsShapesBuffer;

typedef struct PhysicsBuffer {
	PhysicsBodyBuffer body;
	size_t shapesSize; // Sum of the size of fucking everything.

	PhysicsShapesBuffer shapes;
	size_t shapesCount;
} PhysicsBuffer;

typedef struct PhysicsCircleBuffer {
	PhysicsBufferVect offset;
	float r;
	uint32_t padding;
} PhysicsCircleBuffer;

typedef struct PhysicsSegmentBuffer {
	PhysicsBufferVect a;
	PhysicsBufferVect b;
	float r;
	uint32_t padding;
} PhysicsSegmentBuffer;

typedef struct PhysicsPolyBufferHeader {
	uint32_t vectCount;
	float r;
} PhysicsPolyBufferHeader;

typedef struct PhysicsShapeBufferHeader {
	uint32_t type;
	uint32_t size;
} PhysicsShapeBufferHeader;

void * toBufferPhysics(ToBufferArgs args);
void fromBufferPhysics(FromBufferArgs args);

#endif
