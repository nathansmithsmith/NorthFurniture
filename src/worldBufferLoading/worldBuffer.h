#include "world.h"

#ifndef WORLD_BUFFER_H
#define WORLD_BUFFER_H

#define WORLD_HEADER_MAGIC_NUMBER 3445

typedef int32_t WorldInt;
typedef uint32_t WorldUInt;
typedef uint8_t SystemCountInt;
typedef uint16_t WorldComponentSizeInt;

enum {
	WORLD_OPTIONS_NONE = 0x0,
	WORLD_OPTIONS_ALIVE_LIST = 0x1,
	WORLD_OPTIONS_DEAD_LIST = 0x1 << 1,
	WORLD_OPTIONS_CHUNK_MAP = 0x1 << 2
};

typedef uint32_t WorldOption;
typedef uint32_t WorldCheckSum;

typedef struct WorldHeader {
	int32_t magicNumber;
	uint32_t entityCount;

	uint32_t componentsSize;
	WorldOption options;

	WorldCheckSum checksum;
	uint32_t padding;
} WorldHeader;

// We store the fucking world in a shitty buffer.
typedef struct WorldBuffer {
	size_t size;
	void * data;
} WorldBuffer;

// The fucking checksum.
WorldCheckSum runWorldCheckSum(WorldBuffer buffer);

WorldBuffer dumpWorldToBuf(GameData * gameData, World world);
ErrorCodes loadWorld(World * worldOutput, GameData * gameData, WorldBuffer buffer);
void freeWorldBuffer(WorldBuffer * buffer);

#endif
