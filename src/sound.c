#include "sound.h"
#include "util.h"
#include <raylib.h>

ErrorCodes initJukeBox(JukeBox * jukeBox, const Music * songs, size_t songsCount) {
	int i;

	// Allocate songs.
	jukeBox->songs = (Music*)calloc(songsCount, sizeof(Music));

	if (jukeBox->songs == NULL) {
		allocationError("jukeBox->songs");
		return CERROR;
	}

	// Copy songs over.
	jukeBox->songsCount = songsCount;
	
	for (i = 0; i < songsCount; ++i) {
		jukeBox->songs[i] = songs[i];

		// Looping needs to be disabled for it to work correctly.
		jukeBox->songs[i].looping = false;
	}

	jukeBox->mode = JUKEBOX_NORMAL;
	jukeBox->currentSong = 0;
	jukeBox->paused = true;

	TraceLog(LOG_INFO, "Jukebox created with %d songs", songsCount);
	return CSUCCESS;
}

void closeJukeBox(JukeBox * jukeBox) {
	int i;

	// Unload music.
	for (i = 0; i < jukeBox->songsCount; ++i)
		UnloadMusicStream(jukeBox->songs[i]);

	// Free.
	free(jukeBox->songs);

	jukeBox->songsCount = 0;
	jukeBox->songs = NULL;
	jukeBox->currentSong = 0;

	TraceLog(LOG_INFO, "Jukebox closed");
}

void startJukeBox(JukeBox * jukeBox, JukeBoxModes mode) {
	jukeBox->mode = mode;
	jukeBox->paused = false;

	// Set current song.
	if (mode == JUKEBOX_RANDOM) {
		SetRandomSeed(time(NULL));
		jukeBox->currentSong = GetRandomValue(0, jukeBox->songsCount - 1);
	}

	// Play song.
	PlayMusicStream(jukeBox->songs[jukeBox->currentSong]);
}

void resumeJukeBox(JukeBox * jukeBox) {
	Music song = jukeBox->songs[jukeBox->currentSong];
	jukeBox->paused = false;
	ResumeMusicStream(song);
}

void pauseJukeBox(JukeBox * jukeBox) {
	Music song = jukeBox->songs[jukeBox->currentSong];
	jukeBox->paused = true;
	PauseMusicStream(song);
}

void toggleJukeBox(JukeBox * jukeBox) {
	if (jukeBox->paused)
		resumeJukeBox(jukeBox);
	else
		pauseJukeBox(jukeBox);
}

void updateJukeBox(JukeBox * jukeBox) {
	Music song = jukeBox->songs[jukeBox->currentSong];

	UpdateMusicStream(song);

	if (!IsMusicStreamPlaying(song) && !jukeBox->paused)
		nextSongJukeBox(jukeBox);
}

void nextSongJukeBox(JukeBox * jukeBox) {
	// Stop current song and restart.
	Music song = jukeBox->songs[jukeBox->currentSong];
	StopMusicStream(song);

	switch (jukeBox->mode) {
		case JUKEBOX_NORMAL:
			++jukeBox->currentSong;
			jukeBox->currentSong %= jukeBox->songsCount;
			break;
		case JUKEBOX_RANDOM:
			SetRandomSeed(time(NULL));
			jukeBox->currentSong = GetRandomValue(0, jukeBox->songsCount - 1);
			break;
		default:
			break;
	}

	// Play new song.
	song = jukeBox->songs[jukeBox->currentSong];
	PlayMusicStream(song);
}

void initSoundData(SoundData * soundData) {
	soundData->soundsCount = 0;
	soundData->sounds = NULL;

	TraceLog(LOG_INFO, "Sound data created");
}

void closeSoundData(SoundData * soundData) {
	int i;

	if (soundData->sounds == NULL) {
		soundData->soundsCount = 0;
		return;
	}

	// Unload sounds.
	for (i = 0; i < soundData->soundsCount; ++i)
		UnloadSound(soundData->sounds[i]);

	// Free.
	soundData->soundsCount = 0;
	free(soundData->sounds);
	soundData->sounds = NULL;

	TraceLog(LOG_INFO, "Sound data closed");
}

// Not for direct use.
ErrorCodes createAndAddSounds(SoundData * soundData, const Sound * sounds, size_t soundsCount) {
	soundData->soundsCount = soundsCount;

	// Allocate sounds.
	soundData->sounds = (Sound*)calloc(soundsCount, sizeof(Sound));

	if (soundData->sounds == NULL) {
		allocationError("soundData->sounds");
		return CERROR;
	}

	// Copy sounds.
	memcpy(soundData->sounds, sounds, soundsCount * sizeof(Sound));

	return CSUCCESS;
}

ErrorCodes addSounds(SoundData * soundData, const Sound * sounds, size_t soundsCount) {
	int i, j;

	size_t oldSoundCount;

	// Allocates first sounds added.
	if (soundData->sounds == NULL)
		return createAndAddSounds(soundData, sounds, soundsCount);

	oldSoundCount = soundData->soundsCount;
	soundData->soundsCount += soundsCount;

	// Reallocate sounds.
	soundData->sounds = (Sound*)reallocarray(
		soundData->sounds,
		soundData->soundsCount,
		sizeof(Sound)
	);

	if (soundData->sounds == NULL) {
		allocationError("soundData->sounds");
		return CERROR;
	}

	// Copy sounds.
	j = 0;

	for (i = oldSoundCount; i < soundData->soundsCount; ++i)
		soundData->sounds[i] = sounds[j++];

	return CSUCCESS;
}

Sound getSound(SoundData soundData, int id) {
	if (soundData.sounds == NULL || id < 0 || id >= soundData.soundsCount)
		return (Sound){};

	return soundData.sounds[id];
}

ErrorCodes loadSoundsFromFiles(SoundData * soundData, const char ** files, size_t filesSize) {
	int i;
	Sound sounds[filesSize];

	for (i = 0; i < filesSize; ++i)
		sounds[i] = LoadSound(files[i]);

	return addSounds(soundData, sounds, filesSize);
}
