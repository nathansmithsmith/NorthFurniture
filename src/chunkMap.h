#include "gameData.h"
#include "entityMap.h"

#ifndef CHUNK_MAP_H
#define CHUNK_MAP_H

// TODO: Add sonic fast speed

typedef struct ChunkMap {
	EntityList2d chunks;

	// Used for drawing without position component.
	Vector2 currDrawPos;

	RenderTexture2D * renders;
	int * RenderPositions;
	int RendersSize;
} ChunkMap;

ChunkMap getEmptyChunkMap();
ChunkMap createChunkMap(int width, int height);
void freeChunkMap(ChunkMap * chunkMap);

// Updates and draws the fucking chunks.
void updateChunkMap(ChunkMap * chunkMap, World * world, GameData * gameData);

#endif
