// For compiling header only librarys  

#include <raylib.h>

#define RRES_IMPLEMENTATION
#include "rres.h"

#define RRES_RAYLIB_IMPLEMENTATION
#include "rres-raylib.h"

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
