#include "gameData.h"
#include "configLoader.h"
#include "sound.h"
#include "util.h"

// Stuff for loading init.cfg

#ifndef INIT_FILE_LOADER_H
#define INIT_FILE_LOADER_H

#define FILE_LIST_MAX 512

// Used for storing lists of files loaded from a config file.
typedef struct ConfigFileList {
	char listStr[FILE_LIST_MAX];
	char ** files;
	size_t filesSize;
} ConfigFileList;

void initConfigFileList(ConfigFileList * fileList);
void closeConfigFileList(ConfigFileList * fileList);

// Gets 'files' from 'listStr'.
void getFileList(ConfigFileList * fileList);

// Stuff used for loading init.cfg files.
typedef struct InitFileData {
	int jukeBoxMode;

	size_t configLinesSize;
	ConfigLine * configLines;
} InitFileData;

extern const InitFileData defaultInitFileData;

void initInitFileData(InitFileData * initData);
void closeInitFileData(InitFileData * initData);
ConfigErrors loadInitFileDataFromBuf(InitFileData * initData, const char * buf, size_t bufSize);

#endif
