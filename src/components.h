#include "gameData.h"
#include "physics.h"
#include "textureLoader.h"
#include "chunkComponent.h"

#ifndef	COMPONENTS_H
#define COMPONENTS_H

typedef uint16_t EntitySignature; // Bit field full of data on a entity.

#define COMPONENT_COUNT 6

// Bit field stuff for entity signature.
enum {
	NONE_COMPONENT = 0x0,
	POSITION_COMPONENT = 0x1,
	ANGLE_COMPONENT = 0x1 << 1,
	TEXTURE_COMPONENT = 0x1 << 2,
	RECT_COMPONENT = 0x1 << 3,
	PHYSICS_COMPONENT = 0x1 << 4,
	CHUNK_COMPONENT = 0x1 << 5
};

//  
typedef struct EntityComponents {
	Vector2 * position;
	float angle;
	TextureComponent * texture;
	Rectangle * rect;
	PhysicsComponent * physics;
	ChunkComponent * chunk;
} EntityComponents;

EntityComponents getEmptyEntityComponents();

// Creates components from data in signature and returns number of failed components.
// Not for direct use
int createComponentsFromSignature(EntityComponents * components, EntitySignature signature);
void freeEntityComponents(EntityComponents * components);

#endif
