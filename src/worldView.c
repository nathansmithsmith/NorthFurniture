#include "worldView.h"
#include "settings.h"
#include "game.h"

void initWorldView(GameData * gameData, WorldView * worldView) {
	worldView->renderTexture = LoadRenderTexture(SCREEN_WIDTH, SCREEN_HEIGHT);
	resetWorldView(gameData, worldView);
}

void closeWorldView(WorldView * worldView) {
	UnloadRenderTexture(worldView->renderTexture);
}

void resetWorldView(GameData * gameData, WorldView * worldView) {
	Settings Settings = gameData->settings;

	int viewWidth, viewHeight;
	viewWidth = worldView->renderTexture.texture.width;
	viewHeight = worldView->renderTexture.texture.height;

	// Get screen to view window scale.
	float scale;

	if (GetRenderWidth() > GetRenderHeight())
		scale = (float)GetRenderHeight() / viewHeight;
	else
		scale = (float)GetRenderWidth() / viewWidth;

	if (Settings.integerScaling)
		scale = (int)scale;

	worldView->scale = scale;

	float width = viewWidth * scale;
	float height = viewHeight * scale;

	worldView->source = (Rectangle){0.0, 0.0, viewWidth, -viewHeight};

	worldView->dest = (Rectangle){
		(GetRenderWidth() / 2.0) - (width / 2.0),
		(GetRenderHeight() / 2.0) - (height / 2.0),
		width,
		height
	};
}

void drawWorldView(WorldView worldView) {
	DrawTexturePro(
		worldView.renderTexture.texture,
		worldView.source,
		worldView.dest,
		(Vector2){0.0, 0.0},
		0.0,
		WHITE
	);
}
