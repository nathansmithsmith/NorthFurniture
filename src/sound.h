#include "gameData.h"

#ifndef SOUND_H
#define SOUND_H

// Normal plays the songs from top to bottom then repeats
// and random uses a random number generator for the song order.
typedef enum JukeBoxModes {
	JUKEBOX_NORMAL,
	JUKEBOX_RANDOM
} JukeBoxModes;

// The jukebox is like a playlist of songs that play in the background.
typedef struct JukeBox {
	size_t songsCount;
	Music * songs;
	int currentSong;
	JukeBoxModes mode;
	bool paused;
} JukeBox;

ErrorCodes initJukeBox(JukeBox * jukeBox, const Music * songs, size_t songsCount);
void closeJukeBox(JukeBox * jukeBox);

// Should only be called once.
void startJukeBox(JukeBox * jukeBox, JukeBoxModes mode);

void resumeJukeBox(JukeBox * jukeBox);
void pauseJukeBox(JukeBox * jukeBox);
void toggleJukeBox(JukeBox * jukeBox);

void updateJukeBox(JukeBox * jukeBox);
void nextSongJukeBox(JukeBox * jukeBox);

// Used for storing sound effects in the game.
typedef struct SoundData {
	size_t soundsCount;
	Sound * sounds;
} SoundData;

void initSoundData(SoundData * soundData);
void closeSoundData(SoundData * soundData);

ErrorCodes addSounds(SoundData * soundData, const Sound * sounds, size_t soundsCount);

// Gets sound at 'id' in 'sounds'.
Sound getSound(SoundData soundData, int id);

ErrorCodes loadSoundsFromFiles(SoundData * soundData, const char ** files, size_t filesSize);

#endif
