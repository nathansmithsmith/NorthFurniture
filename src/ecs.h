#include "gameData.h"
#include "components.h"

#ifndef ECS_H
#define ECS_H

typedef uint16_t EntityId;
typedef struct EcsData EcsData;

// Arguments to pass to a system.
typedef struct SystemArgs {
	GameData * gameData;
	World * world;
	EntityComponents * componentData;
	EntityId id;
	EntitySignature signature;
} SystemArgs;

// System callback stuff (--:
typedef void (*SYSTEM_CB)(SystemArgs args);

typedef uint16_t SystemId;

// A system and its id.
typedef struct EntitySystem {
	SYSTEM_CB cb;
	SystemId id;
	struct EntitySystem * next;
} EntitySystem;

// A linked list full of systems for a entity 🐱
typedef struct EntitySystems {
	EntitySystem system;
	size_t count;
} EntitySystems;

EntitySystems getEmptyEntitySystems();

// Entities, components, and systems.
typedef struct EcsData {
	size_t entityCount;

	// Entity  
	EntitySignature * entitySignatures; // A signature for each entity.

	// Components.
	EntityComponents * components;

	// Systems.
	EntitySystems * systems;
	int systemCount; // Complete count of all the systems.
} EcsData;

// A fucking messy piece of shit.
ErrorCodes initEcs(EcsData * ecsData, size_t startSize);
ErrorCodes addToEcs(EcsData * ecsData, size_t count);
ErrorCodes popBackEcs(EcsData * ecsData);
void closeEcs(EcsData * ecsData);

// No you stupid fucker!!! Not for direct use.
void setEntityClear(EcsData * ecsData, EntityId id);
ErrorCodes resizeEcs(EcsData * ecsData, size_t entityCount);

void freeSystem(EntitySystems * system);

// Sets the components and signature of a entity.
ErrorCodes setEntityComponents(EcsData ecsData, EntityId id, EntitySignature signature);

// Sets the systems for a entity🐱
ErrorCodes setEntitySystems(EcsData * ecsData, EntityId id, EntitySystem  * systemsList, size_t systemsSize);

EntityComponents * getComponents(EcsData ecsData, EntityId id);
EntitySystems * getSystems(EcsData ecsData, EntityId id);
EntitySignature getSignature(EcsData ecsData, EntityId id);

// Run systems at id.
void runSystems(GameData * gameData, World * world, EntityId id);

#endif
