#include "settings.h"
#include "game.h"
#include "worldView.h"
#include "util.h"

const Settings defaultSettings = {
	.windowWidth = WINDOW_WIDTH,
	.windowHeight = WINDOW_HEIGHT,
	.fullScreen = false,
	.vsync = true,
	.targetFps = 60,
	.drawFps = true,
	.integerScaling = true
};

void initSettings(Settings * settings) {
	const ConfigLine configLines[] = {
		{"windowWidth", &settings->windowWidth, VALUE_INT},
		{"windowHeight", &settings->windowHeight, VALUE_INT},
		{"fullScreen", &settings->fullScreen, VALUE_BOOL},
		{"vsync", &settings->vsync, VALUE_BOOL},
		{"targetFps", &settings->targetFps, VALUE_INT},
		{"drawFps", &settings->drawFps, VALUE_BOOL},
		{"integerScaling", &settings->integerScaling, VALUE_BOOL}
	};

	// Set to default.
	memcpy(settings, &defaultSettings, sizeof(Settings));

	settings->configLinesSize = sizeof(configLines) / sizeof(ConfigLine);
	settings->configLines = allocateConfigLines(configLines, settings->configLinesSize);
	initConfigLines(settings->configLines, settings->configLinesSize);

	TraceLog(LOG_INFO, "Settings created");
}

void closeSettings(Settings * settings) {
	if (settings->configLines != NULL)
		free(settings->configLines);

	TraceLog(LOG_INFO, "Settings closed");
}

ConfigErrors dumpSettings(const char * filePath, const Settings * settings) {
	return dumpConfig(
		filePath, 
		settings->configLines, 
		settings->configLinesSize
	);
}

ConfigErrors loadSettings(GameData * gameData, const char * filePath, Settings * settings) {
	ConfigErrors res = loadConfig(
		filePath, 
		settings->configLines, 
		settings->configLinesSize
	);

	applySettings(gameData, settings);
	return res;
}

void applySettings(GameData * gameData, Settings * settings) {
	unsigned int setFlags, clearFlags; // Window flags.
	setFlags = 0;
	clearFlags = 0;

	// Window.
	SetWindowSize(settings->windowWidth, settings->windowHeight);

	// Full screen.
	if (settings->fullScreen)
		setFlags |= FLAG_FULLSCREEN_MODE;
	else
		clearFlags |= FLAG_FULLSCREEN_MODE;

	// vsync.
	if (settings->vsync)
		setFlags |= FLAG_VSYNC_HINT;
	else {
		clearFlags |= FLAG_VSYNC_HINT;
		SetTargetFPS(settings->targetFps); // Set target fps.
	}

	resetWorldView(gameData, &gameData->worldView);

	ClearWindowState(clearFlags);
	SetWindowState(setFlags);
}
