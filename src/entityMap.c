#include "entityMap.h"
#include "game.h"
#include "util.h"
#include "world.h"

void initEntityMap(EntityMap * entityMap) {
	entityMap->alive = (EntityList){NULL, 0};
	entityMap->dead = (EntityList){NULL, 0};

	TraceLog(LOG_INFO, "Entity map created");
}

void closeEntityMap(EntityMap * entityMap) {
	freeEntityList(&entityMap->alive);
	freeEntityList(&entityMap->dead);

	TraceLog(LOG_INFO, "Entity map closed");
}

ErrorCodes createEntityList(EntityList * entityList, size_t size) {
	entityList->ids = (EntityId*)calloc(size, sizeof(EntityId));

	if (entityList->ids == NULL) {
		allocationError("entityList->ids");
		return CERROR;
	}

	entityList->count = size;
	return CSUCCESS;
}

void freeEntityList(EntityList * entityList) {
	if (entityList->ids != NULL)
		free(entityList->ids);

	*entityList = (EntityList){NULL, 0};
}

ErrorCodes resizeEntityList(EntityList * entityList, size_t size) {

	// Create list if not created.
	if (entityList->ids == NULL)
		return createEntityList(entityList, size);

	// Reallocate.
	entityList->ids = (EntityId*)reallocarray(entityList->ids, size, sizeof(EntityId));

	if (entityList->ids == NULL) {
		allocationError("entityList->ids");
		return CERROR;
	}

	entityList->count = size;
	return CSUCCESS;
}

ErrorCodes addEntityToList(EntityList * entityList, EntityId id) {
	ErrorCodes res = CSUCCESS;
	
	// Make sure count is at 0 if ids is null.
	if (entityList->ids == NULL)
		entityList->count = 0;

	res = resizeEntityList(entityList, entityList->count + 1);
	setEntityInList(entityList, entityList->count - 1, id);
	return res;
}

ErrorCodes setEntityInList(EntityList * entityList, int index, EntityId id) {
	if (entityList->ids == NULL)
		return CERROR;
	if (index < 0 || index >= entityList->count)
		return CERROR;

	entityList->ids[index] = id;
	return CSUCCESS;
}

EntityId getEntityFromList(EntityList entityList, int index) {
	if (entityList.ids == NULL)
		return 0;
	if (index < 0 || index >= entityList.count)
		return 0;

	return entityList.ids[index];
}

ErrorCodes popBackEntityList(EntityList * entityList) {
	// Clean if last item.
	if (entityList->count == 1) {
		freeEntityList(entityList);
		return CSUCCESS;
	}

	return resizeEntityList(entityList, entityList->count - 1);
}

ErrorCodes removeEntityFromListIndex(EntityList * entityList, int index) {
	int i;

	if (entityList->ids == NULL)
		return CERROR;
	if (index < 0 || index >= entityList->count)
		return CERROR;

	// Removing last item.
	if (index == entityList->count - 1)
		return popBackEntityList(entityList);

	// Move items back.
	for (i = index; i < entityList->count - 1; ++i)
		entityList->ids[i] = entityList->ids[i + 1];

	return popBackEntityList(entityList);
}

ErrorCodes removeEntityFromListId(EntityList * entityList, EntityId id) {
	int i;

	if (entityList->ids == NULL)
		return CERROR;

	for (i = 0; i < entityList->count; ++i)
		if (entityList->ids[i] == id)
			return removeEntityFromListIndex(entityList, i);

	return CSUCCESS;
}

void updateEntityList(GameData * gameData, World * world, EntityList entityList) {
	int i;

	if (entityList.ids == NULL)
		return;

	for (i = 0; i < entityList.count; ++i)
		runSystems(gameData, world, entityList.ids[i]);
}

ErrorCodes createEntityList2d(EntityList2d * entityList, int width, int height) {
	entityList->ids = (EntityId*)calloc(width * height, sizeof(EntityId));

	if (entityList->ids == NULL) {
		allocationError("entityList->ids");
		return CERROR;
	}

	entityList->width = width;
	entityList->height = height;
	return CSUCCESS;
}

void freeEntityList2d(EntityList2d * entityList) {
	if (entityList->ids != NULL)
		free(entityList->ids);

	*entityList = (EntityList2d){NULL, 0, 0};
}

ErrorCodes setEntityInList2d(EntityList2d * entityList, Vector2 position, EntityId id) {
	int index;

	if (entityList->ids == NULL)
		return CERROR;
	// Out of bounds.
	if (position.x >= entityList->width || position.x < 0)
		return CERROR;
	if (position.y >= entityList->height || position.y < 0)
		return CERROR;

	index = entityList->width * position.y + position.x;
	entityList->ids[index] = id;
	return CSUCCESS;
}

EntityId getEntityFromList2d(EntityList2d entityList, Vector2 position) {
	int index;

	if (entityList.ids == NULL)
		return 0;
	// Out of bounds.
	if (position.x >= entityList.width || position.x < 0)
		return 0;
	if (position.y >= entityList.height || position.y < 0)
		return 0;

	index = entityList.width * position.y + position.x;
	return entityList.ids[index];
}

void updateEntityListArea2d(GameData * gameData, World * world, EntityList2d entityList, Rectangle area) {
	int x, y;
	int startX, startY; // Where loop should start.
	int endX, endY; // Where loop should end.

	if (entityList.ids == NULL)
		return;

	// Set start values.
	startX = area.x;
	startY = area.y;

	// Set end values.
	endX = area.x + area.width;
	endY = area.y + area.height;

	// Clamp them.
	startX = Clamp(startX, 0, entityList.width - 1);
	startY = Clamp(startY, 0, entityList.height - 1);
	endX = Clamp(endX, 0, entityList.width - 1);
	endY = Clamp(endY, 0, entityList.height - 1);

	// Run systems.
	for (y = startY; y < endY; ++y)
		for (x = startX; x < endX; ++x)
			runSystems(
				gameData,
				world,
				getEntityFromList2d(entityList, (Vector2){x, y})
			);
}

void updateEntityList2d(GameData * gameData, World * world, EntityList2d entityList) {
	int x, y;

	if (entityList.ids == NULL)
		return;

	// Run systems.
	for (y = 0; y < entityList.height; ++y)
		for (x = 0; x < entityList.width; ++x)
			runSystems(
				gameData,
				world,
				getEntityFromList2d(entityList, (Vector2){x, y})
			);
}
