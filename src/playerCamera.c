#include "playerCamera.h"
#include "game.h"
#include "ecs.h"
#include "entityMap.h"
#include "worldView.h"

void initPlayerCamera(Camera2D * camera) {
	camera->target = (Vector2){0.0, 0.0};
	camera->offset = (Vector2){0.0, 0.0};
	camera->rotation = 0.0;
	camera->zoom = 1.0;
}

void updatePlayerCamera(GameData * gameData, Camera2D * camera) {
	EntityComponents * player = getComponents(gameData->world->ecsData, 1);

	if (player == NULL)
		return;

	float speed = 1000.0 * GetFrameTime();

	if (IsKeyDown(KEY_UP))
		camera->target.y -= speed;
	if (IsKeyDown(KEY_DOWN))
		camera->target.y += speed;
	if (IsKeyDown(KEY_RIGHT))
		camera->target.x += speed;
	if (IsKeyDown(KEY_LEFT))
		camera->target.x -= speed;

	if (IsKeyDown(KEY_A))
		camera->zoom += 0.01;
	if (IsKeyDown(KEY_B))
		camera->zoom -= 0.01;

	return;

	int viewWidth, viewHeight;
	viewWidth = SCREEN_WIDTH;
	viewHeight = SCREEN_HEIGHT;

	camera->offset = (Vector2){viewWidth / 2.0, viewHeight / 2.0};
	camera->target = *player->position;
}
