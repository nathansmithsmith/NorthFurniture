#include "mainMenu.h"
#include "game.h"
#include <raylib.h>
#include <stdbool.h>

MainMenuData mainMenuData = {
	.nextState = CURRENT_STATE
};

void mainMenuInit(STATE_CALLBACK_ARGS) {
	MainMenuData * menu = (MainMenuData*)data;

	resizeMenu(gameData, menu);
}

void mainMenuClose(STATE_CALLBACK_ARGS) {
}

StateIds mainMenuUpdate(STATE_CALLBACK_ARGS) {
	MainMenuData * menu = (MainMenuData*)data;

	// Handle resize.
	if (IsWindowResized())
		resizeMenu(gameData, menu);

	return menu->nextState;
}

void mainMenuDraw(STATE_CALLBACK_ARGS) {
	MainMenuData * menu = (MainMenuData*)data;

	// Draw logo.
	DrawTextureEx(
		menu->logoTexture,
		(Vector2){menu->logoRect.x, menu->logoRect.y},
		0.0,
		menu->logoScale,
		WHITE
	);

	// Start button.
	if (GuiButton(menu->startButton, "Start"))
		menu->nextState = GAME_PLAY_STATE;

	// Level editor button.
	if (GuiButton(menu->levelEditorButton, "Level editor"))
		menu->nextState = LEVEL_EDITOR_STATE;

	// Quit button.
	if (GuiButton(menu->quitButton, "Quit"))
		gameData->gameOver = true;
}

void resizeMenu(GameData * gameData, MainMenuData * menu) {
	int winWidth = GetRenderWidth();
	int winHeight = GetRenderHeight();
	
	int buttonWidth = winWidth / 10.0;
	int buttonHeight = winWidth / 16.0;
	int buttonScreenCenter = (winWidth / 2.0) - (buttonWidth / 2.0);
	int buttonGap = 5;

	// Logo.
	menu->logoTexture = getTexture(gameData->textureData, LOGO_TEXTURE);
	menu->logoScale = winWidth / menu->logoTexture.width / 2;
	menu->logoRect.width = menu->logoTexture.width * menu->logoScale;
	menu->logoRect.height = menu->logoTexture.height * menu->logoScale;
	menu->logoRect.x = (winWidth / 2.0) - (menu->logoRect.width / 2.0);
	menu->logoRect.y = (winHeight / 2.0) - menu->logoRect.height;

	// Start button.
	menu->startButton = (Rectangle){
		buttonScreenCenter,
		menu->logoRect.height + menu->logoRect.y + buttonGap,
		buttonWidth,
		buttonHeight
	};

	// Level editor button.
	menu->levelEditorButton = (Rectangle){
		buttonScreenCenter,
		menu->startButton.height + menu->startButton.y + buttonGap,
		buttonWidth,
		buttonHeight
	};

	// Quit button.
	menu->quitButton = (Rectangle){
		buttonScreenCenter,
		menu->levelEditorButton.height + menu->levelEditorButton.y + buttonGap,
		buttonWidth,
		buttonHeight
	};
}
