#include "gamePlay.h"
#include "game.h"
#include "playerCamera.h"
#include "ecs.h"
#include "entityMap.h"
#include "util.h"
#include "physics.h"
#include "sound.h"
#include "chunkMap.h"

GamePlayData gamePlayData = {
};

void gamePlayInit(STATE_CALLBACK_ARGS) {
	GamePlayData * game = (GamePlayData*)data;
}

void gamePlayClose(STATE_CALLBACK_ARGS) {
	GamePlayData * game = (GamePlayData*)data;
}

StateIds gamePlayUpdate(STATE_CALLBACK_ARGS) {
	GamePlayData * game = (GamePlayData*)data;

	updatePlayerCamera(gameData, &gameData->world->playerCamera);
	cpSpaceStep(gameData->world->physicsSpace, GetFrameTime());

	if (IsKeyPressed(KEY_SPACE))
		toggleJukeBox(&gameData->jukeBox);

	if (IsKeyPressed(KEY_ONE))
		PlaySound(getSound(gameData->soundData, 0));
	if (IsKeyPressed(KEY_TWO))
		PlaySound(getSound(gameData->soundData, 1));
	if (IsKeyPressed(KEY_THREE))
		PlaySound(getSound(gameData->soundData, 2));
	if (IsKeyPressed(KEY_FOUR))
		PlaySound(getSound(gameData->soundData, 3));
	if (IsKeyPressed(KEY_FIVE))
		PlaySound(getSound(gameData->soundData, 4));

	updateJukeBox(&gameData->jukeBox);

	return CURRENT_STATE;
}

void gamePlayDraw(STATE_CALLBACK_ARGS) {
	GamePlayData * game = (GamePlayData*)data;

	BeginTextureMode(gameData->worldView.renderTexture);
	BeginMode2D(gameData->world->playerCamera);

	ClearBackground(BLACK);

	//updateEntityList2d(
	//	gameData,
	//	&gameData->world->ecsData,
	//	gameData->world->entityMap.chunckMap
	//);
	
	updateChunkMap(
		&gameData->world->chunkMap,
		gameData->world,
		gameData
	);

	updateEntityList(
		gameData,
		gameData->world,
		gameData->world->entityMap.alive
	);

	if (gameData->physicsDebugDraw)
		drawPhysicsSpace(gameData->world->physicsSpace);

	EndMode2D();
	EndTextureMode();

	drawWorldView(gameData->worldView);
}
