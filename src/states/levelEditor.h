#include "gameData.h"
#include "gameStates.h"

#ifndef LEVEL_EDITOR_H
#define LEVEL_EDITOR_H

typedef struct LevelEditorData {
} LevelEditorData;

extern LevelEditorData levelEditorData;

void levelEditorInit(STATE_CALLBACK_ARGS);
void levelEditorClose(STATE_CALLBACK_ARGS);
StateIds levelEditorUpdate(STATE_CALLBACK_ARGS);
void levelEditorDraw(STATE_CALLBACK_ARGS);

#endif
