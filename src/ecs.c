#include "ecs.h"
#include "util.h"
#include "world.h"

EntitySystems getEmptyEntitySystems() {
	return (EntitySystems){(EntitySystem){NULL, 0, NULL}, 0};
}

ErrorCodes initEcs(EcsData * ecsData, size_t startSize) {
	int i;

	// Allocate memory.
	ecsData->entitySignatures = (EntitySignature*)calloc(startSize, sizeof(EntitySignature));
	ecsData->components = (EntityComponents*)calloc(startSize, sizeof(EntityComponents));
	ecsData->systems = (EntitySystems*)calloc(startSize, sizeof(EntitySystems));

	if (ecsData->entitySignatures == NULL ||
        ecsData->components == NULL ||
        ecsData->systems == NULL) {
		allocationError("ecsData");
		return CERROR;
	}

	ecsData->entityCount = startSize;
	ecsData->systemCount = 0;

	// Fill with init values.
	for (i = 0; i < startSize; ++i)
		setEntityClear(ecsData, i);

	TraceLog(LOG_INFO, "Ecs created with a entity count of %ld", ecsData->entityCount);
	return CSUCCESS;
}

ErrorCodes addToEcs(EcsData * ecsData, size_t count) {
	return resizeEcs(ecsData, ecsData->entityCount + count);
}

ErrorCodes popBackEcs(EcsData * ecsData) {
	return resizeEcs(ecsData, ecsData->entityCount - 1);
}

void setEntityClear(EcsData * ecsData, EntityId id) {
	ecsData->entitySignatures[id] = NONE_COMPONENT;
	ecsData->components[id] = getEmptyEntityComponents();
	ecsData->systems[id] = getEmptyEntitySystems();
}

ErrorCodes resizeEcs(EcsData * ecsData, size_t entityCount) {
	int i;

	if (ecsData->entityCount <= 0)
		return CERROR;
	if (entityCount <= 0)
		return CERROR;

	size_t oldEntityCount = ecsData->entityCount;
	ecsData->entityCount = entityCount;

	// Allocate.
	
	// Signatures.
	ecsData->entitySignatures = (EntitySignature*)reallocarray(
		ecsData->entitySignatures,
		entityCount,
		sizeof(EntitySignature)
	);

	// Components.
	ecsData->components = (EntityComponents*)reallocarray(
		ecsData->components,
		entityCount,
		sizeof(EntityComponents)
	);

	// Systems.
	ecsData->systems = (EntitySystems*)reallocarray(
		ecsData->systems,
		entityCount,
		sizeof(EntitySystems)
	);

	// Error allocating.
	if (ecsData->entitySignatures == NULL ||
        ecsData->components == NULL ||
        ecsData->systems == NULL) {
		allocationError("ecsData");
		return CERROR;
	}

	if (entityCount > oldEntityCount)
		for (i = oldEntityCount; i < entityCount; ++i)
			setEntityClear(ecsData, i);

	return CSUCCESS;
}

void closeEcs(EcsData * ecsData) {
	int i;

	// Free signatures.
	if (ecsData->entitySignatures != NULL)
		free(ecsData->entitySignatures);

	// Free components and systems.
	for (i = 0; i < ecsData->entityCount; ++i) {
		freeEntityComponents(getComponents(*ecsData, i));
		freeSystem(getSystems(*ecsData, i));
	}

	if (ecsData->components != NULL)
		free(ecsData->components);
	if (ecsData->systems != NULL)
		free(ecsData->systems);

	ecsData->entityCount = 0;
	ecsData->entitySignatures = NULL;
	ecsData->components = NULL;
	ecsData->systems = NULL;

	TraceLog(LOG_INFO, "Ecs closed");
}

void freeSystem(EntitySystems * system) {
	EntitySystem * systemData = system->system.next;
	EntitySystem * lastSystem = NULL;

	while (systemData != NULL) {
		lastSystem = systemData;
		systemData = systemData->next;
		free(lastSystem);
	}

	*system = getEmptyEntitySystems();
}

ErrorCodes setEntityComponents(EcsData ecsData, EntityId id, EntitySignature signature) {
	EntityComponents * components = NULL;
	ErrorCodes res = CSUCCESS; // Return value.

	components = getComponents(ecsData, id);

	if (components == NULL)
		return CERROR;

	// Free old components.
	freeEntityComponents(components);

	// Set signature.
	ecsData.entitySignatures[id] = signature;

	// Create components.
	if (createComponentsFromSignature(components, signature) > 0) {
		TraceLog(
			LOG_WARNING, 
			"Error creating components for entity %d with signature %x",
			id,
			signature
		);

		res = CERROR;
	}

	return res;
}

ErrorCodes setEntitySystems(EcsData * ecsData, EntityId id, EntitySystem  * systemsList, size_t systemsSize) {
	int i;
	EntitySystems * systems = getSystems(*ecsData, id);
	EntitySystem * currentSystem = &systems->system;

	if (systems == NULL)
		return CERROR;

	// Free old systems.
	freeSystem(systems);

	systems->count = systemsSize;
	ecsData->systemCount += systemsSize;

	// Allocate and set systems.
	for (i = 0; i < systemsSize; ++i) {
		currentSystem->cb = systemsList[i].cb;
		currentSystem->id = systemsList[i].id;

		// Near end.
		if (i == systemsSize - 1) {
			currentSystem->next = NULL;
			continue;
		}

		// Allocate next.
		currentSystem->next = (EntitySystem*)malloc(sizeof(EntitySystem));

		if (currentSystem->next == NULL) {
			allocationError("currentSystem->next");
			return CERROR;
		}

		currentSystem = currentSystem->next;
	}

	return CSUCCESS;
}

EntityComponents * getComponents(EcsData ecsData, EntityId id) {
	if (ecsData.components == NULL || id >= ecsData.entityCount || id < 0)
		return NULL;

	return &ecsData.components[id];
}

EntitySystems * getSystems(EcsData ecsData, EntityId id) {
	if (ecsData.systems == NULL || id >= ecsData.entityCount || id < 0)
		return NULL;

	return &ecsData.systems[id];
}

EntitySignature getSignature(EcsData ecsData, EntityId id) {
	if (ecsData.entitySignatures == NULL || id >= ecsData.entityCount || id < 0)
		return NONE_COMPONENT;

	return ecsData.entitySignatures[id];
}

void runSystems(GameData * gameData, World * world, EntityId id) {
	EntitySystem * systems = NULL;

	SystemArgs args = {
		.gameData = gameData,
		.world = world,
		.componentData = getComponents(world->ecsData, id),
		.id = id,
		.signature = getSignature(world->ecsData, id)
	};

	// Get systems.
	systems = &getSystems(world->ecsData, id)->system;

	// Run systems.
	while (systems != NULL) {
		if (systems->cb != NULL)
			systems->cb(args);

		systems = systems->next;
	}
}
