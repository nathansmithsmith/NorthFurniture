#include "gameData.h"
#include "gameStates.h"
#include "settings.h"
#include "textureLoader.h"
#include "physics.h"
#include "ecs.h"
#include "entityMap.h"
#include "worldView.h"
#include "entitySystems.h"
#include "sound.h"
#include "world.h"

#ifndef GAME_H
#define GAME_H

// Yo momma so fat when she tells a joke the only thing that cracks is the ground.

// Data for the entire game.
typedef struct GameData {

	// Game state.
	StateIds stateId;
	GameState * state;

	Settings settings;
	GameTextureData textureData;
	EntitySystemTable systemTable;
	WorldList worlds;
	World * world;
	WorldView worldView;
	JukeBox jukeBox;
	SoundData soundData;

	// Game flags.
	bool gameOver;
	bool physicsDebugDraw;

} GameData;

// Starting and closing.
void initGame(GameData * gameData, int argc, char ** argv);
void closeGame(GameData * gameData);

void gameWindowResized(GameData * gameData);

// Updating and drawing.
void updateGame(GameData * gameData);
void drawGame(GameData * gameData);

void runGame(GameData * gameData, int argc, char ** argv);

#endif
