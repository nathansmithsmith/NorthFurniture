#include "game.h"
#include "util.h"
#include "playerCamera.h"
#include "entitySystems.h"
#include "assetsListLoader.h"
#include "worldBufferLoading/componentToFromBuffer.h"
#include "worldBufferLoading/worldBuffer.h"
#include "chunkComponent.h"
#include <sys/stat.h>
#include <chipmunk/chipmunk_structs.h>

void entityStuff(GameData * gameData) {
	EntityComponents * component = NULL;

	EntitySystem systems[] = {
		getSystemFromTable(gameData->systemTable, DRAW_SYSTEM_RECT),
		getSystemFromTable(gameData->systemTable, TEST_STSTEM),
		getSystemFromTable(gameData->systemTable, PHYSICS_SYSTEM)
	};

	// Entity 0.
	setEntityComponents(
		gameData->world->ecsData,
		0,
		POSITION_COMPONENT | RECT_COMPONENT | TEXTURE_COMPONENT | PHYSICS_COMPONENT
	);

	component = getComponents(gameData->world->ecsData, 0);
	*component->position = (Vector2){32, -100};
	*component->rect = (Rectangle){0, 0, 30, 34};

	setTextureComponent(component->texture, gameData->textureData, 2);

	cpFloat mass = 1.0;

	component->physics->body = cpSpaceAddBody(
		gameData->world->physicsSpace,
		cpBodyNew(mass, cpMomentForBox(mass, component->rect->width, component->rect->height))
	);

	setVelocityCb(component->physics, 0);
	setPositionCb(component->physics, 0);

	cpBodySetPosition(
		component->physics->body, 
		cpv(component->position->x, component->position->y)
	);

	cpShape * shape1 = cpBoxShapeNew(
		component->physics->body,
		component->rect->width,
		component->rect->height,
		0.0
	);

	cpSpaceAddShape(gameData->world->physicsSpace, shape1);

	const cpShape * shapes1[] = {shape1};
	setShapesInPhysicsComponent(component->physics, shapes1, 1);

	setEntitySystems(
		&gameData->world->ecsData,
		0,
		systems,
		sizeof(systems) / sizeof(EntitySystem)
	);

	// Entity 1.
	setEntityComponents(
		gameData->world->ecsData,
		1,
		POSITION_COMPONENT | RECT_COMPONENT | TEXTURE_COMPONENT | PHYSICS_COMPONENT
	);

	component = getComponents(gameData->world->ecsData, 1);
	*component->position = (Vector2){0, 16};
	*component->rect = (Rectangle){0, 0, 136, 53};

	setTextureComponent(component->texture, gameData->textureData, 0);

	//component->physics->body = cpSpaceAddBody(gameData->world->physicsSpace, cpBodyNewStatic());
	component->physics->body = cpSpaceGetStaticBody(gameData->world->physicsSpace);
	component->physics->usingStaticBody = true;

	cpShape * logoShape = cpSegmentShapeNew(
		component->physics->body,
		cpv(component->position->x - component->rect->width / 2.0, component->position->y - component->rect->height / 2.0),
		cpv(component->position->x + component->rect->width, component->position->y),
		5
	);

	cpSpaceAddShape(gameData->world->physicsSpace, logoShape);

	const cpShape * logoShapes[] = {logoShape};
	setShapesInPhysicsComponent(component->physics, logoShapes, 1);

	EntitySystem systems2[] = {
		getSystemFromTable(gameData->systemTable, DRAW_SYSTEM_RECT),
		getSystemFromTable(gameData->systemTable, PHYSICS_SYSTEM)
	};

	setEntitySystems(
		&gameData->world->ecsData,
		1,
		systems2,
		sizeof(systems2) / sizeof(EntitySystem)
	);

	// Entity 2.
	setEntityComponents(
		gameData->world->ecsData,
		2,
		POSITION_COMPONENT | ANGLE_COMPONENT | PHYSICS_COMPONENT
	);

	component = getComponents(gameData->world->ecsData, 2);
	*component->position = (Vector2){0, 0};
	component->angle = 45;

	cpVect verts[] = {
		cpv(0.5, -2.0),
		cpv(0.0, 1.0),
		cpv(1.0, 1.0),
	};

	size_t vertsSize = sizeof(verts) / sizeof(cpVect);

	for (int i = 0; i < vertsSize; ++i) {
		verts[i].x *= 10;
		verts[i].y *= 10;

		verts[i].x -= 20;
		verts[i].y -= 70;
	}

	component->physics->body = cpSpaceAddBody(
		gameData->world->physicsSpace,
		cpBodyNew(10.0, cpMomentForPoly(10.0, vertsSize, verts, cpvzero, 0.0))
	);

	component->physics->usingStaticBody = false;

	cpShape * polyThing = cpPolyShapeNewRaw(
		component->physics->body,
		vertsSize,
		verts,
		0.0
	); cpSpaceAddShape(gameData->world->physicsSpace, polyThing);

	const cpShape * polyThingShapes[] = {polyThing};
	setShapesInPhysicsComponent(component->physics, polyThingShapes, 1);

	// Entity 3.
	setEntityComponents(
		gameData->world->ecsData,
		3,
		POSITION_COMPONENT | ANGLE_COMPONENT | PHYSICS_COMPONENT
	);

	component = getComponents(gameData->world->ecsData, 3);
	*component->position = (Vector2){0, 0};
	component->angle = 45;

	component->physics->body = cpSpaceAddBody(
		gameData->world->physicsSpace,
		cpBodyNew(1.0, cpMomentForCircle(1.0, 15.0, 15.0, cpvzero))
	);

	component->physics->usingStaticBody = false;

	cpShape * circleDude = cpCircleShapeNew(
		component->physics->body,
		15,
		cpv(0.0, -30.0)
	);

	cpSpaceAddShape(gameData->world->physicsSpace, circleDude);

	const cpShape * circleDudeShapes[] = {circleDude};
	setShapesInPhysicsComponent(component->physics, circleDudeShapes, 1);

	// Chunks.
	int x, y, i;

	EntitySystem chunkSystems[] = {
		getSystemFromTable(gameData->systemTable, DRAW_CHUNK_SYSTEM)
	};

	gameData->world->chunkMap = createChunkMap(255, 255);

	ChunkInt positions[CHUNK_TILE_COUNT] = {
		0, 0, 0, 0, 0,
		0, 1, 2, 3, 0,
		0, 1, 2, 3, 0,
		0, 1, 2, 3, 0,
		0, 0, 0, 0, 0
	};

	i = 4;

	for (y = 0; y < 255; ++y)
		for (x = 0; x < 255; ++x) {
			setEntityComponents(
				gameData->world->ecsData,
				i,
				CHUNK_COMPONENT
			);

			component = getComponents(gameData->world->ecsData, i);
			component->chunk->textureId = 4;
			memcpy(component->chunk->positions, positions, sizeof(ChunkInt) * CHUNK_TILE_COUNT);

			setEntitySystems(
				&gameData->world->ecsData,
				i,
				chunkSystems,
				1
			);

			setEntityInList2d(
				&gameData->world->chunkMap.chunks,
				(Vector2){x, y},
				i
			);

			++i;
		}
}

void initGame(GameData * gameData, int argc, char ** argv) {

	// Init game flags.
	gameData->gameOver = false;
	gameData->physicsDebugDraw = true;

	// Open window.
	InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, argv[0]);

	// Settings.
	initSettings(&gameData->settings);
	applySettings(gameData, &gameData->settings);

	//dumpSettings("settings.set", &gameData->settings);
	loadSettings(gameData, "settings.set", &gameData->settings);

	// Test texture loader.
	initTextureData(&gameData->textureData);

	const char * textureFiles[255] = {
		"images/NorthFurnitureLogo.png",
		"images/evil_puppy_right_1.png",
		"images/cyborg.png",
		"images/test_chunk.png",
		"images/spriteSheet.png"
	};

	loadTexturesFromFiles(
		&gameData->textureData, 
		textureFiles, 
		5
	);

	// Game state.
	initStates(gameData);
	gameData->stateId = MAIN_MENU_STATE;
	setState(MAIN_MENU_STATE, gameData);

	Texture2D spriteSheet = getTexture(gameData->textureData, 0);
	int x, y;

	// System table.
	initEntitySystemTable(&gameData->systemTable);

	// World.
	gameData->worlds = createWorldList(1);
	gameData->world = &gameData->worlds.worlds[0];

	//*gameData->world = createWorld(4 + (255*255));

	//cpSpaceSetGravity(gameData->world->physicsSpace, cpv(0, 20));

	//addEntityToList(&gameData->world->entityMap.alive, 0);
	//addEntityToList(&gameData->world->entityMap.alive, 1);

	//entityStuff(gameData);
	
	// Test loading.
	
	// Get size and allocate.
	struct stat st;
	stat("testWorld.w", &st);
	WorldBuffer worldBuffer;

	worldBuffer.size = st.st_size;
	worldBuffer.data = malloc(worldBuffer.size);

	// Load from file.
	FILE * fp = fopen("testWorld.w", "r");
	fread(worldBuffer.data, worldBuffer.size, 1, fp);
	fclose(fp);

	// Load from buffer.
	loadWorld(&gameData->worlds.worlds[0], gameData, worldBuffer);
	gameData->world = &gameData->worlds.worlds[0];

	cpSpaceSetGravity(gameData->world->physicsSpace, cpv(0, 20));

	// Free.
	freeWorldBuffer(&worldBuffer);

	// World view.
	initWorldView(gameData, &gameData->worldView);

	// Sound.
	InitAudioDevice();

	// Jukebox.
	Music songs[] = {
		LoadMusicStream("sound/song1.mp3"),
		LoadMusicStream("sound/song2.mp3")
	};

	initJukeBox(&gameData->jukeBox, songs, sizeof(songs) / sizeof(Music));
	startJukeBox(&gameData->jukeBox, JUKEBOX_NORMAL);

	// Sound data.
	initSoundData(&gameData->soundData);

	const char * soundFiles[255] = {
		"sound/sound.wav",
		"sound/spring.wav"
	};

	const char * soundFiles2[255] = {
		"sound/coin.wav",
		"sound/weird.wav"
	};

	loadSoundsFromFiles(&gameData->soundData, soundFiles, 2);
	loadSoundsFromFiles(&gameData->soundData, soundFiles2, 2);

	// Testing world.
	//WorldBuffer worldBuf;
	//worldBuf = dumpWorldToBuf(gameData, *gameData->world);

	//FILE * worldFp = fopen("testWorld.w", "w");
	//fwrite(worldBuf.data, worldBuf.size, 1, worldFp);
	//fclose(worldFp);

	//freeWorldBuffer(&worldBuf);
}

void closeGame(GameData * gameData) {
	closeStates(gameData);
	closeSettings(&gameData->settings);
	closeTextureData(&gameData->textureData);
	closeEntitySystemTable(&gameData->systemTable);
	freeWorldList(&gameData->worlds);
	closeWorldView(&gameData->worldView);
	closeJukeBox(&gameData->jukeBox);
	closeSoundData(&gameData->soundData);

	// Keep close window and audio at end.
	CloseWindow();
	CloseAudioDevice();
}

void gameWindowResized(GameData * gameData) {
	resetWorldView(gameData, &gameData->worldView);
}

void updateGame(GameData * gameData) {
	updateState(gameData);

	if (IsWindowResized())
		gameWindowResized(gameData);
}

void drawGame(GameData * gameData) {
	BeginDrawing();

	ClearBackground(RAYWHITE);

	drawState(gameData);

	// Draw fps.
	if (gameData->settings.drawFps)
		DrawFPS(0, 0);

	EndDrawing();
}

void runGame(GameData * gameData, int argc, char ** argv) {
	// Start game.
	initGame(gameData, argc, argv);

	// Run update loop
	while (!WindowShouldClose()) {
		updateGame(gameData);
		drawGame(gameData);

		if (gameData->gameOver)
			break;
	}

	// Close game.
	closeGame(gameData);
}
