#include "assetsListLoader.h"
#include "util.h"

void initAssetsList(AssetsList * assetsList) {
	assetsList->files = NULL;
	assetsList->filesCount = 0;
}

void closeAssetsList(AssetsList * assetsList) {
	int i;

	// Free memory.
	if (assetsList->files == NULL) {
		for (i = 0; i < assetsList->filesCount; ++i)
			free(assetsList->files[i]);

		free(assetsList->files);
	}

	assetsList->files = NULL;
	assetsList->filesCount = 0;
}

ErrorCodes addAssetFile(AssetsList * assetsList, const char * fileName, size_t fileNameSize) {
	int i;

	// Add files count.
	++assetsList->filesCount;

	// Allocate 'files'.
	if (assetsList->files == NULL)
		assetsList->files = (char**)calloc(assetsList->filesCount, sizeof(char*));
	else
		assetsList->files = (char**)reallocarray(assetsList->files, assetsList->filesCount, sizeof(char*));

	if (assetsList->files == NULL) {
		allocationError("none");
		return CERROR;
	}

	i = assetsList->filesCount - 1;

	assetsList->files[i] = (char*)calloc(fileNameSize, sizeof(char));

	if (assetsList->files[i] == NULL) {
		allocationError("none");
		return CERROR;
	}

	// Copy over.
	strncpy(assetsList->files[i], fileName, fileNameSize);

	return CSUCCESS;
}

ErrorCodes loadAssetFilesFromBuf(AssetsList * assetsList, const char * buf, size_t bufSize) {
	int position = 0;
	
	char lineBuf[ASSETS_LIST_NAME_MAX];

	// Read lines.
	while (true) {
		position = readLineFromBuf(
			buf,
			bufSize,
			lineBuf,
			ASSETS_LIST_NAME_MAX,
			position
		);

		if (lineBuf[0] == '\0' && position != -1)
			continue;
		else if (lineBuf[0] == '\0')
			break;

		// Add file.
		if (addAssetFile(assetsList, lineBuf, strnlen(lineBuf, ASSETS_LIST_NAME_MAX) + 1) != CSUCCESS)
			return CERROR;

		if (position == -1)
			break;
	}

	return CSUCCESS;
}
