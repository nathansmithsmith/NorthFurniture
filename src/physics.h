#include "gameData.h"
#include <chipmunk/chipmunk.h>

#ifndef PHYSICS_H
#define PHYSICS_H

typedef struct PhysicsComponent {
	cpBody * body;
	bool usingStaticBody; // Is using static body from space.
	cpShape ** shapes;
	size_t shapesCount;

	// Callback ids.
	int velocityCb;
	int positionCb;
} PhysicsComponent;

// Init physics component with 'shapesCount' shapes.
PhysicsComponent * createPhysicsComponent();

#define PHYSICS_CB_ENTRY_NAME_MAX 25

// Free body and shapes in component.
void freePhysicsComponent(PhysicsComponent * component);

// Sets the shapes in component.
ErrorCodes setShapesInPhysicsComponent(PhysicsComponent * component, const cpShape ** shapes, size_t shapesCount);

#define DEFAULT_PHYSICS_CB -1

typedef struct VelocityFuncEntry {
	char name[PHYSICS_CB_ENTRY_NAME_MAX];
	cpBodyVelocityFunc func;
} VelocityFuncEntry;

typedef struct PositionFuncEntry {
	char name[PHYSICS_CB_ENTRY_NAME_MAX];
	cpBodyPositionFunc func;
} PositionFuncEntry;

cpBodyVelocityFunc setVelocityCb(PhysicsComponent * component, int id);
cpBodyPositionFunc setPositionCb(PhysicsComponent * component, int id);

// Callbacks for velocity updating.
#define VELOCITY_UPDATE_CBS_SIZE 1
extern const VelocityFuncEntry velocityUpdateCbs[VELOCITY_UPDATE_CBS_SIZE];

// Callbacks for position updating.
#define POSITION_UPDATE_CBS_SIZE 1
extern const PositionFuncEntry positionUpdateCbs[POSITION_UPDATE_CBS_SIZE];

void updatePositionNoAngle(cpBody * body, cpFloat dt);
void updateVelocityNoAngle(cpBody * body, cpVect gravity, cpFloat damping, cpFloat dt);

Color toRayColor(cpSpaceDebugColor color);
Vector2 toRayVector2(cpVect vect);

// Draw functions for debugging physics.
void debugDrawCircle(cpVect p, cpFloat a, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data);
void debugDrawSegment(cpVect a, cpVect b, cpSpaceDebugColor color, cpDataPointer data);
void debugDrawFatSegment(cpVect a, cpVect b, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data);
void debugDrawPolygon(int count, const cpVect *verts, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data);
void debugDrawDot(cpFloat size, cpVect pos, cpSpaceDebugColor color, cpDataPointer data);
cpSpaceDebugColor debugColorForShape(cpShape *shape, cpDataPointer data);

// Used for debugging physics😼
void drawPhysicsSpace(cpSpace * space);

#endif
