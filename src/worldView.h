#include "gameData.h"

#ifndef WORLD_VIEW_H
#define WORLD_VIEW_H

typedef struct WorldView {
	RenderTexture2D renderTexture;
	Rectangle source;
	Rectangle dest;
	float scale;
} WorldView;

void initWorldView(GameData * gameData, WorldView * worldView);
void closeWorldView(WorldView * worldView);

// If window resized or settings changed.
void resetWorldView(GameData * gameData, WorldView * worldView);

void drawWorldView(WorldView worldView);

#endif
